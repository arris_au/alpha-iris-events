<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('events', '\AlphaIris\Events\Http\Controllers\Frontend\EventController@index')->name('events.index');
    Route::get('events/{event}', '\AlphaIris\Events\Http\Controllers\Frontend\EventController@show')->name('events.show');
    Route::post('events/{event}/cart', '\AlphaIris\Events\Http\Controllers\Frontend\EventController@store')->name('events.cart');
    Route::get('events/{event}/cart', '\AlphaIris\Events\Http\Controllers\Frontend\EventController@cart')->name('events.show_cart');
    Route::post('events/{event}/register', '\AlphaIris\Events\Http\Controllers\Frontend\EventController@register')->name('events.register');
    Route::get('events/{event}/rsvp', '\AlphaIris\Events\Http\Controllers\Frontend\EventController@showRsvp')->name('events.rsvp');
    Route::post('events/{event}/rsvp', '\AlphaIris\Events\Http\Controllers\Frontend\EventController@rsvp')->name('events.sendrsvp');
});

Route::group(['middleware' => ['web'], 'as' => 'voyager.'], function () {
    Route::get('admin/admin/events/{id}/replicate', [
        'uses' => '\AlphaIris\Events\Http\Controllers\EventController@replicate',
        'as' => 'events.replicate',
    ]);
    Route::get('ticket-sales/{ticketSale}/checkin', [
        'uses' => '\AlphaIris\Events\Http\Controllers\TicketSaleController@toggleCheckIn',
        'as' => 'ticket-sales.checkin',
    ]);
    Route::get('admin/event-fields/event/{event}/ticket/{ticket}', [
        'uses' => '\AlphaIris\Events\Http\Controllers\EventFieldsController@forEventTicket',
        'as' => 'event-fields.for-event-ticket',
    ]);
});
