<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

class AddPrivate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->uuid('uuid')->after('id');
            $table->boolean('is_private')->after('needs_rsvp');
            $table->index('uuid');
        });

        foreach (\AlphaIris\Events\Models\Event::all() as $event) {
            $event->uuid = Str::uuid();
            $event->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->dropColumn('is_private');
            $table->dropColumn('uuid');
        });
    }
}
