<?php

use AlphaIris\Payments\Services\PaymentsService;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('venues', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('address_1')->nullable();
            $table->string('address_2')->nullable();
            $table->string('suburb')->nullable();
            $table->foreignId('state_id')->nullable();
            $table->string('postcode')->nullable();
            $table->foreignId('country_id')->nullable();
            $table->timestamps();

            $table->foreign('state_id')->references('id')->on('states');
            $table->foreign('country_id')->references('id')->on('countries');
        });

        Schema::create('events', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('description')->nullable();
            $table->dateTime('event_start');
            $table->dateTime('event_end');
            $table->boolean('is_ticketed')->default(false);
            $table->foreignId('venue_id');
            $table->timestamps();

            $table->foreign('venue_id')->references('id')->on('venues');
            $table->index('event_start');
            $table->index('event_end');
            $table->index('name');
        });

        Schema::create('tickets', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('description')->nullable();
            $table->foreignId('event_id');
            $table->float('price')->default(0);
            $table->text('membership_type_req')->nullable();
            $table->integer('limit')->nullable();
            $table->dateTime('available_from')->nullable();
            $table->dateTime('available_to')->nullable();
            $table->integer('ticket_order')->nullable();
            $table->timestamps();

            $table->index('price');
            $table->index('name');
            $table->foreign('event_id')->references('id')->on('events');
        });

        Schema::create('ticket_sales', function (Blueprint $table) {
            $table->id();
            $table->foreignId('event_id');
            $table->foreignId('ticket_id');
            $table->foreignId('user_id')->nullable();
            $table->float('ticket_price');
            $table->integer('order_payment_status')->default(PaymentsService::PAYMENT_PROCESSING);
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email');
            $table->string('phone');
            $table->string('address_1')->nullable();
            $table->string('address_2')->nullable();
            $table->string('suburb')->nullable();
            $table->foreignId('state_id')->nullable();
            $table->string('postcode')->nullable();
            $table->foreignId('country_id')->nullable();
            $table->timestamps();

            $table->foreign('state_id')->references('id')->on('states');
            $table->foreign('country_id')->references('id')->on('countries');
            $table->foreign('event_id')->references('id')->on('events');
            $table->foreign('ticket_id')->references('id')->on('tickets');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_sales');
        Schema::dropIfExists('tickets');
        Schema::dropIfExists('events');
        Schema::dropIfExists('venues');
    }
}
