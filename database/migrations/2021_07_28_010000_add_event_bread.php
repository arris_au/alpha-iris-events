<?php

use AlphaIris\Payments\Services\PaymentsService;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;
use TCG\Voyager\Models\DataRow;
use TCG\Voyager\Models\DataType;
use TCG\Voyager\Models\Menu;
use TCG\Voyager\Models\MenuItem;
use TCG\Voyager\Models\Permission;
use TCG\Voyager\Models\Role;

class AddEventBread extends Migration
{
    protected $ticketSaleRows = [
        'event_id' => [
            'field' => 'event_id',
            'type' => 'number',
            'display_name' => 'event_id',
            'read' => true,
            'edit' => true,
            'add' => true,
        ],
        [
            'field' => 'ticket_sale_belongsto_event_relationship',
            'type' => 'relationship',
            'display_name' => 'Event',
            'browse' => true,
            'read' => true,
            'edit' => true,
            'add' => true,
            'details' => [
               'model' => '\\AlphaIris\\Events\\Models\\Event',
               'table' => 'events',
               'type' => 'belongsTo',
               'column' => 'event_id',
               'key' => 'id',
               'label' => 'name',
               'pivot_table' => 'attribute_values',
               'pivot' => false,
               'taggable' => false,
            ],
        ],
        'ticket_id' => [
            'field' => 'ticket_id',
            'type'=> 'number',
            'display_name' => 'ticket_id',
            'read' => true,
            'edit' => true,
            'add' => true,
        ],
        [
            'field' => 'ticket_sale_belongsto_ticket_relationship',
            'type' => 'relationship',
            'display_name' => 'Ticket Type',
            'browse' => true,
            'read' => true,
            'edit' => true,
            'add' => true,
            'details' => [
               'model' => '\\AlphaIris\\Events\\Models\\Ticket',
               'table' => 'events',
               'type' => 'belongsTo',
               'column' => 'ticket_id',
               'key' => 'id',
               'label' => 'name',
               'pivot_table' => 'attribute_values',
               'pivot' => false,
               'taggable' => false,
            ],
        ],
        'user_id' => [
            'field' => 'user_id',
            'type' => 'number',
            'display_name' => 'user_id',
            'read' => true,
            'edit' => true,
            'add' => true,
        ],
        [
            'field' => 'ticket_sale_belongsto_user_relationship',
            'type' => 'relationship',
            'display_name' => 'User',
            'browse' => true,
            'read' => true,
            'edit' => true,
            'add' => true,
            'details' => [
               'model' => '\\AlphaIris\\Core\\Models\\User',
               'table' => 'users',
               'type' => 'belongsTo',
               'column' => 'user_id',
               'key' => 'id',
               'label' => 'name',
               'pivot_table' => 'attribute_values',
               'pivot' => false,
               'taggable' => false,
            ],
        ],
        'order_payment_status' => [
            'field' => 'order_payment_status',
            'type' => 'select_dropdown',
            'display_name' => 'Payment Status',
            'required' => true,
            'read' => true,
            'add' => true,
            'edit' => true,
            'details' => [
                'default' => PaymentsService::PAYMENT_PROCESSING,
                'options' =>[
                    PaymentsService::PAYMENT_PROCESSING => 'Processing',
                    PaymentsService::PAYMENT_CLEARED => 'Cleared',
                    PaymentsService::PAYMENT_CANCELLED => 'Cancelled',
                    PaymentsService::PAYMENT_DECLINED => 'Declined',
                ],
            ],

        ],
        'first_name' => [
            'field' => 'first_name',
            'type' => 'text',
            'display_name' => 'First Name',
            'browse' => true,
            'required' => true,
            'read' => true,
            'add' => true,
            'edit' => true,

        ],
        'last_name' => [
            'field' => 'last_name',
            'type' => 'text',
            'display_name' => 'Last Name',
            'browse' => true,
            'required' => true,
            'read' => true,
            'add' => true,
            'edit' => true,
        ],

        'email' => [
            'field' => 'email',
            'type' => 'text',
            'display_name' => 'Email',
            'browse' => true,
            'required' => true,
            'read' => true,
            'add' => true,
            'edit' => true,
        ],

        'phone' => [
            'field' => 'phone',
            'type' => 'text',
            'display_name' => 'Phone',
            'browse' => true,
            'required' => true,
            'read' => true,
            'add' => true,
            'edit' => true,
        ],
        'address_1' => [
            'field' => 'address_1',
            'type' => 'text',
            'display_name' => 'Address 1',
            'read' => true,
            'add' => true,
            'edit' => true,
        ],
        'address_2' => [
            'field' => 'address_2',
            'type' => 'text',
            'display_name' => 'Address 2',
            'read' => true,
            'add' => true,
            'edit' => true,
        ],
        'suburb' => [
            'field' => 'suburb',
            'type' => 'text',
            'display_name' => 'Suburb',
            'read' => true,
            'add' => true,
            'edit' => true,
        ],
        'state_id' => [
            'field' => 'state_id',
            'type' => 'number',
            'display_name' => 'state_id',
            'read' => true,
            'edit' => true,
            'add' => true,
        ],
        [
            'field' => 'ticket_sale_belongsto_state_relationship',
            'type' => 'relationship',
            'display_name' => 'State',
            'read' => true,
            'edit' => true,
            'add' => true,
            'details' => [
               'model' => '\\AlphaIris\\Core\\Models\\State',
               'table' => 'states',
               'type' => 'belongsTo',
               'column' => 'state_id',
               'key' => 'id',
               'label' => 'name',
               'pivot_table' => 'attribute_values',
               'pivot' => false,
               'taggable' => false,

            ],
        ],
        'postcode' => [
            'field' => 'postcode',
            'type' => 'text',
            'display_name' => 'Postcode',
            'read' => true,
            'edit' => true,
            'add' => true,
        ],
        'country_id' => [
            'field' => 'country_id',
            'type' => 'number',
            'display_name' => 'country_id',
            'read' => true,
            'edit' => true,
            'add' => true,
        ],
        [
            'field' => 'ticket_sale_belongsto_country_relationship',
            'type' => 'relationship',
            'display_name' => 'Country',
            'read' => true,
            'edit' => true,
            'add' => true,
            'details' => [
               'model' => '\\AlphaIris\\Core\\Models\\Country',
               'table' => 'countries',
               'type' => 'belongsTo',
               'column' => 'country_id',
               'key' => 'id',
               'label' => 'name',
               'pivot_table' => 'attribute_values',
               'pivot' => false,
               'taggable' => false,

            ],
        ],
    ];

    protected $ticketRSVPRows = [
        'event_id' => [
            'field' => 'event_id',
            'type' => 'number',
            'display_name' => 'event_id',
            'read' => true,
            'edit' => true,
            'add' => true,
        ],
        [
            'field' => 'ticket_rsvp_belongsto_event_relationship',
            'type' => 'relationship',
            'display_name' => 'Event',
            'browse' => true,
            'read' => true,
            'edit' => true,
            'add' => true,
            'details' => [
               'model' => '\\AlphaIris\\Events\\Models\\Event',
               'table' => 'events',
               'type' => 'belongsTo',
               'column' => 'event_id',
               'key' => 'id',
               'label' => 'name',
               'pivot_table' => 'attribute_values',
               'pivot' => false,
               'taggable' => false,
            ],
        ],
        'user_id' => [
            'field' => 'user_id',
            'type' => 'number',
            'display_name' => 'user_id',
            'read' => true,
            'edit' => true,
            'add' => true,
        ],
        [
            'field' => 'ticket_rsvp_belongsto_user_relationship',
            'type' => 'relationship',
            'display_name' => 'User',
            'browse' => true,
            'read' => true,
            'edit' => true,
            'add' => true,
            'details' => [
               'model' => '\\AlphaIris\\Core\\Models\\User',
               'table' => 'users',
               'type' => 'belongsTo',
               'column' => 'user_id',
               'key' => 'id',
               'label' => 'name',
               'pivot_table' => 'attribute_values',
               'pivot' => false,
               'taggable' => false,
            ],
        ],
        'first_name' => [
            'field' => 'first_name',
            'type' => 'text',
            'display_name' => 'First Name',
            'browse' => true,
            'required' => true,
            'read' => true,
            'add' => true,
            'edit' => true,

        ],
        'last_name' => [
            'field' => 'last_name',
            'type' => 'text',
            'display_name' => 'Last Name',
            'browse' => true,
            'required' => true,
            'read' => true,
            'add' => true,
            'edit' => true,
        ],

        'email' => [
            'field' => 'email',
            'type' => 'text',
            'display_name' => 'Email',
            'browse' => true,
            'required' => true,
            'read' => true,
            'add' => true,
            'edit' => true,
        ],

        'phone' => [
            'field' => 'phone',
            'type' => 'text',
            'display_name' => 'Phone',
            'browse' => true,
            'required' => true,
            'read' => true,
            'add' => true,
            'edit' => true,
        ],
        'address_1' => [
            'field' => 'address_1',
            'type' => 'text',
            'display_name' => 'Address 1',
            'read' => true,
            'add' => true,
            'edit' => true,
        ],
        'address_2' => [
            'field' => 'address_2',
            'type' => 'text',
            'display_name' => 'Address 2',
            'read' => true,
            'add' => true,
            'edit' => true,
        ],
        'suburb' => [
            'field' => 'suburb',
            'type' => 'text',
            'display_name' => 'Suburb',
            'read' => true,
            'add' => true,
            'edit' => true,
        ],
        'state_id' => [
            'field' => 'state_id',
            'type' => 'number',
            'display_name' => 'state_id',
            'read' => true,
            'edit' => true,
            'add' => true,
        ],
        [
            'field' => 'ticket_rsvp_belongsto_state_relationship',
            'type' => 'relationship',
            'display_name' => 'State',
            'read' => true,
            'edit' => true,
            'add' => true,
            'details' => [
               'model' => '\\AlphaIris\\Core\\Models\\State',
               'table' => 'states',
               'type' => 'belongsTo',
               'column' => 'state_id',
               'key' => 'id',
               'label' => 'name',
               'pivot_table' => 'attribute_values',
               'pivot' => false,
               'taggable' => false,

            ],
        ],
        'postcode' => [
            'field' => 'postcode',
            'type' => 'text',
            'display_name' => 'Postcode',
            'read' => true,
            'edit' => true,
            'add' => true,
        ],
        'country_id' => [
            'field' => 'country_id',
            'type' => 'number',
            'display_name' => 'country_id',
            'read' => true,
            'edit' => true,
            'add' => true,
        ],
        [
            'field' => 'ticket_rsvp_belongsto_country_relationship',
            'type' => 'relationship',
            'display_name' => 'Country',
            'read' => true,
            'edit' => true,
            'add' => true,
            'details' => [
               'model' => '\\AlphaIris\\Core\\Models\\Country',
               'table' => 'countries',
               'type' => 'belongsTo',
               'column' => 'country_id',
               'key' => 'id',
               'label' => 'name',
               'pivot_table' => 'attribute_values',
               'pivot' => false,
               'taggable' => false,

            ],
        ],
    ];

    protected $ticketRows = [
        'name' => [
            'field' => 'name',
            'type' => 'text',
            'display_name' => 'Name',
            'required' => true,
            'browse' => true,
            'read' => true,
            'edit' => true,
            'add' => true,
        ],
        'description' => [
            'field' => 'description',
            'type' => 'text_area',
            'display_name' => 'Description',
            'read' => true,
            'edit' => true,
            'add' => true,
        ],
        'available_from' => [
            'field' => 'available_from',
            'type' => 'datetime',
            'display_name' => 'On Sale From (leave blank for immediate)',
            'read' => true,
            'edit' => true,
            'add' => true,
        ],
        'available_to' => [
            'field' => 'available_to',
            'type' => 'datetime',
            'display_name' => 'On Sale To (leave blank for no end date)',
            'read' => true,
            'edit' => true,
            'add' => true,
        ],
        'ticket_order' => [
            'field' => 'ticket_order',
            'type' => 'number',
            'display_name'=> 'Display Order',
            'read' => true,
            'edit' => true,
            'add' => true,
        ],
        'event_id' => [
            'field' => 'event_id',
            'type' => 'number',
            'display_name' => 'event_id',
            'read' => true,
            'edit' => true,
            'add' => true,
        ],
        [
            'field' => 'ticket_belongsto_event_relationship',
            'type' => 'relationship',
            'display_name' => 'Event',
            'browse' => true,
            'read' => true,
            'edit' => true,
            'add' => true,
            'details' => [
               'model' => '\\AlphaIris\\Events\\Models\\Event',
               'table' => 'events',
               'type' => 'belongsTo',
               'column' => 'event_id',
               'key' => 'id',
               'label' => 'name',
               'pivot_table' => 'attribute_values',
               'pivot' => false,
               'taggable' => false,
            ],
        ],
        'price' => [
            'field' => 'price',
            'type' => 'number',
            'display_name' => 'Price',
            'browse' => true,
            'read' => true,
            'edit' => true,
            'add' => true,
        ],
        'membership_type_req' => [
            'field' => 'membership_type_req',
            'type' => 'text_area',
            'display_name' => 'Limit to Membership Type(s)',
            'read' => true,
            'edit' => true,
            'add' => true,
            'details' => [
                'view' => 'alpha-iris-payments::membership_type_req_field',
            ],
            ],
        'limit' => [
            'field' => 'limit',
            'type' => 'number',
            'display_name' => 'Limit (0 for unlimited)',
            'read' => true,
            'edit' => true,
            'add' => true,
        ],
    ];

    protected $eventRows = [
        'name' => [
            'field' =>'name',
            'type' => 'text',
            'display_name' => 'Name',
            'required' => true,
            'browse' => true,
            'read' => true,
            'edit' => true,
            'add' => true,
        ],
        'description' => [
            'field' => 'description',
            'type' => 'text_area',
            'display_name' => 'Description',
            'read' => true,
            'edit' => true,
            'add' => true,
        ],
        'event_start' => [
            'field' => 'event_start',
            'type' => 'datetime',
            'display_name' => 'Event Start',
            'browse' => true,
            'required' => true,
            'read' => true,
            'edit' => true,
            'add' => true,
        ],
        'event_end' => [
            'field' => 'event_end',
            'type' => 'datetime',
            'display_name' => 'Event End',
            'required' => true,
            'browse' => true,
            'read' => true,
            'edit' => true,
            'add' => true,
        ],
        'is_ticketed' => [
            'field' => 'is_ticketed',
            'display_name' => 'Is Ticketed',
            'type' => 'checkbox',
            'read' => true,
            'edit' => true,
            'add' => true,
        ],

        'venue_id' => [
            'field' => 'venue_id',
            'type' => 'number',
            'display_name' => 'venue_id',
            'browse' => false,
            'read' => true,
            'edit' => true,
            'add' => true,
        ],
        [
            'field' => 'event_belongsto_venue_relationship',
            'type' => 'relationship',
            'display_name' => 'Venue',
            'browse' => true,
            'read' => true,
            'edit' => true,
            'add' => true,
            'required' => true,
            'details' => [
               'model' => '\\AlphaIris\\Events\\Models\\Venue',
               'table' => 'venues',
               'type' => 'belongsTo',
               'column' => 'venue_id',
               'key' => 'id',
               'label' => 'name',
               'pivot_table' => 'attribute_values',
               'pivot' => false,
               'taggable' => false,
            ],
        ],
    ];

    protected $venueRows = [
        'name' => [
            'field' => 'name',
            'type' => 'text',
            'display_name' => 'Name',
            'required' => true,
            'browse' => true,
            'read' => true,
            'edit' => true,
            'add' => true,
        ],
        'description' => [
            'field' => 'description',
            'type' => 'text_area',
            'display_name' => 'Description',
            'read' => true,
            'edit' => true,
            'add' => true,
        ],
        'address_1' => [
            'field' => 'address_1',
            'type' => 'text',
            'display_name' => 'Address 1',
            'read' => true,
            'edit' => true,
            'add' => true,
        ],
        'address_2' => [
            'field' => 'address_2',
            'type' => 'text',
            'display_name' => 'Address 2',
            'read' => true,
            'edit' => true,
            'add' => true,
        ],
        'suburb' => [
            'field' => 'suburb',
            'type' => 'text',
            'display_name' => 'Suburb',
            'read' => true,
            'edit' => true,
            'add' => true,
        ],
        'country_id' => [
            'field' => 'country_id',
            'type' => 'number',
            'display_name' => 'country_id',
            'browse' => true,
            'read' => true,
            'edit' => true,
            'add' => true,
        ],

        'state_id' => [
            'field' => 'state_id',
            'type' => 'number',
            'display_name' => 'state_id',
            'read' => true,
            'edit' => true,
            'add' => true,
        ],
        [
            'field' => 'venue_belongsto_state_relationship',
            'type' => 'relationship',
            'display_name' => 'State',
            'read' => true,
            'edit' => true,
            'add' => true,
            'details' => [
               'model' => '\\AlphaIris\\Core\\Models\\State',
               'table' => 'states',
               'type' => 'belongsTo',
               'column' => 'state_id',
               'key' => 'id',
               'label' => 'name',
               'pivot_table' => 'attribute_values',
               'pivot' => false,
               'taggable' => false,

            ],
        ],
        'postcode' => [
            'field' => 'postcode',
            'type' => 'text',
            'display_name' => 'Postcode',
            'read' => true,
            'edit' => true,
            'add' => true,
        ],
        [
            'field' => 'venue_belongsto_country_relationship',
            'type' => 'relationship',
            'display_name' => 'Country',
            'read' => true,
            'edit' => true,
            'add' => true,
            'details' => [
               'model' => '\\AlphaIris\\Core\\Models\\Country',
               'table' => 'countries',
               'type' => 'belongsTo',
               'column' => 'country_id',
               'key' => 'id',
               'label' => 'name',
               'pivot_table' => 'attribute_values',
               'pivot' => false,
               'taggable' => false,

            ],
        ],
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $venueDT = $this->createFillDT('Venue', 'Venues', 'voyager-company');
        $venueDT->save();
        $this->createFillDR($venueDT, $this->venueRows);
        Permission::generateFor('venues');

        $eventDT = $this->createFillDT('Event', 'Events', 'voyager-group');
        $eventDT->save();
        $this->createFillDR($eventDT, $this->eventRows);
        Permission::generateFor('events');

        $ticketDT = $this->createFillDT('Ticket', 'Tickets', 'voyager-ticket');
        $ticketDT->save();
        $this->createFillDR($ticketDT, $this->ticketRows);
        Permission::generateFor('tickets');

        $ticketSaleDT = $this->createFillDT('Ticket Sale', 'Ticket Sales', 'voyager-credit-cards');
        $ticketSaleDT->save();
        $this->createFillDR($ticketSaleDT, $this->ticketSaleRows);
        Permission::generateFor('ticket_sales');

        $ticketSaleRSVP = $this->createFillDT('Ticket RSVPs', 'Ticket RSVPs', 'voyager-credit-cards', 'ticket-rsvps');
        $ticketSaleRSVP->save();
        $this->createFillDR($ticketSaleRSVP, $this->ticketRSVPRows);
        Permission::generateFor('ticket_rsvps');

        // All all the new permissions to the admin role
        $role = Role::where('name', 'admin')->firstOrFail();
        $permissions = Permission::all();
        $role->permissions()->sync(
            $permissions->pluck('id')->all()
        );

        $adminMenu = Menu::where('name', 'admin')->firstOrFail();
        $eventsMenu = [[
            'title' => 'Events',
            'url' => '',
            'target' => '_self',
            'icon_class' => 'voyager-group',
            'color' => null,
            'route' => null,
            'parameters' => null,
            'children' => [
              [
                'title' => 'Events',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-group',
                'color' => null,
                'route' => 'voyager.events.index',
                'parameters' => null,
                ],
                [
                    'title' => 'Ticket Types',
                    'url' => '',
                    'target' => '_self',
                    'icon_class' => 'voyager-ticket',
                    'color' => null,
                    'route' => 'voyager.tickets.index',
                    'parameters' => null,
                    ],
                [
                    'title' => 'Ticket Sales',
                    'url' => '',
                    'target' => '_self',
                    'icon_class' => 'voyager-credit-cards',
                    'color' => null,
                    'route' => 'voyager.ticket-sales.index',
                    'parameters' => null,
                ],
                [
                    'title' => 'Venues',
                    'url' => '',
                    'target' => '_self',
                    'icon_class' => 'voyager-company',
                    'color' => null,
                    'route' => 'voyager.venues.index',
                    'parameters' => null,
                ],
            ],
        ]];
        $this->populateMenu($adminMenu, $eventsMenu);
    }

    protected function removeMenu($menuItem)
    {
        if ($menuItem->children->count() > 0) {
            foreach ($menuItem->children as $child) {
                $this->removeMenu($child);
            }
        }
        $menuItem->delete();
    }

    protected function populateMenu($menu, $menuItems, $parentId = null)
    {
        foreach ($menuItems as $index => $menuItem) {
            $newItem = MenuItem::create([
              'title' => $menuItem['title'],
              'url' => $menuItem['url'],
              'target' => $menuItem['target'],
              'icon_class' => $menuItem['icon_class'],
              'color' => $menuItem['color'],
              'order' => $index + 1,
              'route' => $menuItem['route'],
              'parameters' => $menuItem['parameters'] ? json_encode($menuItem['parameters']) : null,
              'parent_id' => $parentId,
              'menu_id' => $menu->id,
            ]);

            if (isset($menuItem['children'])) {
                $this->populateMenu($menu, $menuItem['children'], $newItem->id);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->deleteDT('venues', 'venues');
        $this->deleteDT('events', 'events');
        $this->deleteDT('tickets', 'tickets');
        $this->deleteDT('ticket_sales', 'ticket_sales');

        $this->removePermissions('venues');
        $this->removePermissions('events');
        $this->removePermissions('tickets');
        $this->removePermissions('ticket_sales');

        $eventsItems = MenuItem::where('title', 'Events')->get();
        foreach ($eventsItems as $item) {
            $this->removeMenu($item);
        }
    }

    protected function createFillDR($dt, $rows)
    {
        $order = 1;
        foreach ($rows as $row) {
            $dtRow = $this->dataRow($dt, $row['field']);
            $dtRow->fill([
                'field' => $row['field'],
                'type' => $row['type'],
                'display_name' => $row['display_name'],
                'required' => isset($row['required']) ? $row['required'] : false,
                'browse' => isset($row['browse']) ? $row['browse'] : false,
                'read' => isset($row['read']) ? $row['read'] : false,
                'edit' => isset($row['edit']) ? $row['edit'] : false,
                'add' => isset($row['add']) ? $row['add'] : false,
                'delete' => isset($row['delete']) ? $row['delete'] : false,
                'order' => $order,
                'details' => isset($row['details']) ? ($row['details']) : '',
            ]);
            $dtRow->save();
            $order++;
        }
    }

    protected function dataRow($type, $field)
    {
        return DataRow::firstOrNew([
            'data_type_id' => $type->id,
            'field'        => $field,
        ]);
    }

    protected function createFillDT($singular, $plural, $icon, $slug = null, $name = null, $model = null, $controller = null, $generatePermissions = true, $serverSide = true)
    {
        $slug = $slug ?: Str::slug($plural);
        $name = $name ?: str_replace('-', '_', $slug);
        $model = $model ?: 'AlphaIris\\Events\\Models\\'.str_replace(' ', '', $singular);
        $controller = $controller ?: 'AlphaIris\\Events\\Http\\Controllers\\'.str_replace(' ', '', $singular).'Controller';

        $dtModel = DataType::firstOrNew([
            'slug' => $slug,
            'name' => $name,
        ]);

        $dtModel->fill([
            'display_name_singular' => $singular,
            'display_name_plural' => $plural,
            'icon' => $icon,
            'model_name' => $model,
            'controller' => $controller,
            'generate_permissions' => $generatePermissions,
            'server_side' => $serverSide,
        ]);

        return $dtModel;
    }

    protected function deleteDT($name, $slug)
    {
        $dt = DataType::where('name', $name)->where('slug', $slug)->first();
        if ($dt) {
            $dt->rows()->delete();
            $dt->delete();
        }
    }

    protected function removePermissions($tableName)
    {
        Permission::where('table_name', $tableName)->delete();
    }
}
