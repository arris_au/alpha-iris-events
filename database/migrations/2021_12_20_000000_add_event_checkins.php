<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEventCheckins extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ticket_rsvps', function (Blueprint $table) {
            $table->dateTime('checked_in')->after('user_id')->nullable();
        });

        Schema::table('ticket_sales', function (Blueprint $table) {
            $table->dateTime('checked_in')->after('user_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ticket_rsvps', function (Blueprint $table) {
            $table->dropColumn('checked_in');
        });

        Schema::table('ticket_sales', function (Blueprint $table) {
            $table->dropColumn('checked_in');
        });
    }
}
