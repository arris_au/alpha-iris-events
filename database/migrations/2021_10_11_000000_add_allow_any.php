<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAllowAny extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tickets', function (Blueprint $table) {
            $table->boolean('allow_any_member')->after('membership_type_req');
            $table->boolean('hide_unauth')->after('membership_type_req');
        });

        Schema::table('events', function (Blueprint $table) {
            $table->boolean('needs_rsvp')->after('is_ticketed');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tickets', function (Blueprint $table) {
            $table->dropColumn('allow_any_member');
            $table->dropColumn('hide_unauth');
        });

        Schema::table('events', function (Blueprint $table) {
            $table->dropColumn('needs_rsvp');
        });
    }
}
