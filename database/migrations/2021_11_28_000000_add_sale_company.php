<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

class AddSaleCompany extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ticket_sales', function (Blueprint $table) {
            $table->string('company')->before('first_name')->nullable();
        });

        Schema::table('ticket_rsvps', function (Blueprint $table) {
            $table->string('company')->before('first_name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ticket_sales', function (Blueprint $table) {
            $table->dropColumn('company');
        });

        Schema::table('ticket_rsvps', function (Blueprint $table) {
            $table->dropColumn('company');
        });
    }
}
