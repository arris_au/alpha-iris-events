<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

class AddEventFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_fields', function (Blueprint $table) {
            $table->id();
            $table->foreignId('event_id');
            $table->string('field');
            $table->string('type');
            $table->string('display_name');
            $table->boolean('required')->default(false);
            $table->boolean('browse');
            $table->boolean('read');
            $table->boolean('edit');
            $table->boolean('add');
            $table->boolean('delete');
            $table->text('details');
            $table->integer('order');
            $table->timestamps();

            $table->foreign('event_id')->references('id')->on('events')->cascadeOnDelete();
        });

        Schema::create('event_values', function (Blueprint $table) {
            $table->id();
            $table->foreignId('attribute_id');
            $table->foreignId('model_id');
            $table->text('string_value')->nullable();
            $table->text('text_value')->nullable();
            $table->datetime('dt_value')->nullable();
            $table->double('float_value', 8, 2)->nullable();
            $table->boolean('bool_value')->nullable();
            $table->timestamps();

            $table->foreign('attribute_id')->references('id')->on('event_fields')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('event_values');
        Schema::drop('event_fields');
    }
}
