<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use TCG\Voyager\Models\Setting;

class AddEmailSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $setting = $this->findSetting('events.send_sales_email');
        if (! $setting->exists) {
            $setting->fill([
                'display_name' => 'Send Emails for ticket purchases',
                'value'        => '1',
                'details'      => '',
                'type'         => 'checkbox',
                'order'        => 1,
                'group'        => 'Events',
            ])->save();
        }

        $setting = $this->findSetting('events.send_rsvp_email');
        if (! $setting->exists) {
            $setting->fill([
                'display_name' => 'Send Emails for event RSVPs',
                'value'        => '1',
                'details'      => '',
                'type'         => 'checkbox',
                'order'        => 2,
                'group'        => 'Events',
            ])->save();
        }
        $setting = $this->findSetting('events.cc_site_owner');
        if (! $setting->exists) {
            $setting->fill([
                'display_name' => 'CC Site Owner on all emails',
                'value'        => '1',
                'details'      => '',
                'type'         => 'checkbox',
                'order'        => 3,
                'group'        => 'Events',
            ])->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $keys = ['events.cc_site_owner', 'events.send_sales_email', 'events.send_rsvp_email'];
        foreach ($keys as $key) {
            $setting = $this->findSetting($key);
            if ($setting->exists) {
                $setting->delete();
            }
        }
    }

    protected function findSetting($key)
    {
        return Setting::firstOrNew(['key' => $key]);
    }
}
