@extends('voyager::bread.edit-add')

@push('footerScripts')
    <script>
        function generateField() {
            $('input[name=field]').val(
                $('input[name=display_name]').val()
                    .toLowerCase()
                    .replace(/[^a-z0-9_\s]/g, "")
                    .replace(/\s\s+/g, ' ')
                    .trim()
                    .replace(/\s/g, '_')
            );
        }

        $('input[name=display_name]').on('change keyup', function () {
            generateField();
        });
    </script>
@endpush