@php
    $edit = !is_null($dataTypeContent->getKey());
    $add = is_null($dataTypeContent->getKey());
@endphp
@extends('voyager::bread.edit-add')

@section('css')
    <style>
        .nav-tabs {
            background: none !important;
            border-bottom: 0px;
        }

        .nav-tabs .active a {
            border: 0px;
        }

        .nav-tabs>li {
            margin-bottom: -1px !important;
        }

        .nav-tabs a {
            text-align: center;
            background: #f8f8f8;
            border: 1px solid #f1f1f1 !important;
            position: relative;
            top: -1px;
            border-bottom-left-radius: 0px;
            border-bottom-right-radius: 0px;
        }

        .nav-tabs a i {
            display: block;
            font-size: 22px;
        }

        .tab-content {
            background: #ffffff;
            border: 1px solid transparent;
        }

        .tab-content>div {
            padding: 10px;
        }

        .nav-tabs>li.active>a,
        .nav-tabs>li.active>a:focus,
        .nav-tabs>li.active>a:hover {
            background: #fff !important;
            color: #62a8ea !important;
            border-bottom: 1px solid #fff !important;
            top: -1px !important;
        }

        .nav-tabs>li a {
            transition: all 0.3s ease;
        }


        .nav-tabs>li.active>a:focus {
            top: 0px !important;
        }

        .nav-tabs>li>a:hover {
            background-color: #fff !important;
        }
    </style>
@stop

@if ($edit)
    @section('content')
        <div class="page-content edit-add container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-bordered settings">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a
                                    data-toggle="tab"
                                    href="#event-details"
                                >Event Details</a>
                            </li>
                            @if (!$dataTypeContent->needs_rsvp)
                                <li>
                                    <a
                                        data-toggle="tab"
                                        href="#ticket-types"
                                    >Ticket Types</a>
                                </li>
                                <li>
                                    <a
                                        data-toggle="tab"
                                        href="#ticket-sales"
                                    >Ticket Sales</a>
                                </li>
                            @else
                                <li>
                                    <a
                                        data-toggle="tab"
                                        href="#ticket-sales"
                                    >Ticket RSVPs</a>
                                </li>
                            @endif
                        </ul>
                        <div class="tab-content">
                            <div
                                id="event-details"
                                class="tab-pane active"
                            >
                                <p class="p-4">
                                    URL: <a
                                        href="{{ route('events.show', $dataTypeContent) }}"
                                        target="_blank"
                                    >{{ route('events.show', $dataTypeContent) }}</a>
                                </p>
                                @include('voyager::bread.partials.edit-add-form')
                            </div>
                            @if (!$dataTypeContent->needs_rsvp)
                                <div
                                    id="ticket-types"
                                    class="tab-pane "
                                >
                                    <livewire:alpha-iris-ticket-table
                                        data-type="tickets"
                                        per-page="5"
                                        event_id="{{ $dataTypeContent->id }}"
                                    />
                                </div>
                            @endif
                            <div
                                id="ticket-sales"
                                class="tab-pane"
                            >
                                @if ($dataTypeContent->needs_rsvp)
                                    <livewire:alpha-iris-rsvp-table
                                        data-type="ticket-rsvps"
                                        per-page="10"
                                        event_id="{{ $dataTypeContent->id }}"
                                    />
                                @else
                                    <livewire:alpha-iris-ticket-sales-table
                                        data-type="ticket-sales"
                                        per-page="10"
                                        can-search="true"
                                        exportable="true"
                                        event-id="{{ $dataTypeContent->id }}"
                                    />
                                @endif
                            </div>
                        </div>

                        <iframe
                            id="form_target"
                            name="form_target"
                            style="display:none"
                        ></iframe>
                        <form
                            id="my_form"
                            action="{{ route('voyager.upload') }}"
                            target="form_target"
                            method="post"
                            enctype="multipart/form-data"
                            style="width:0;height:0;overflow:hidden"
                        >
                            <input
                                name="image"
                                id="upload_file"
                                type="file"
                                onchange="$('#my_form').submit();this.value='';"
                            >
                            <input
                                type="hidden"
                                name="type_slug"
                                id="type_slug"
                                value="{{ $dataType->slug }}"
                            >
                            {{ csrf_field() }}
                        </form>

                    </div>
                </div>
            </div>
        </div>

        <div
            class="modal fade modal-danger"
            id="confirm_delete_modal"
        >
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <button
                            type="button"
                            class="close"
                            data-dismiss="modal"
                            aria-hidden="true"
                        >&times;</button>
                        <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}
                        </h4>
                    </div>

                    <div class="modal-body">
                        <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'
                        </h4>
                    </div>

                    <div class="modal-footer">
                        <button
                            type="button"
                            class="btn btn-default"
                            data-dismiss="modal"
                        >{{ __('voyager::generic.cancel') }}</button>
                        <button
                            type="button"
                            class="btn btn-danger"
                            id="confirm_delete"
                        >{{ __('voyager::generic.delete_confirm') }}</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Delete File Modal -->
    @stop
@endif

@push('footerScripts')
    <script>
        $(function () {
            toggleRsvpLimitField();
        });

        $('#event_start_date').change(function () {
            if ($('#event_end_date').val() == '') {
                $('#event_end_date').val($('#event_start_date').val());
            }
        });

        $('#needs_rsvp').change(function () {
            toggleRsvpLimitField();
        });

        function toggleRsvpLimitField() {
            var show = $('#needs_rsvp').prop('checked');
            $('input[name=rsvp_limit').closest('.form-group').toggle(show);
            if (!show) {
                $('input[name=rsvp_limit').val('');
            }
        }
    </script>
@endpush
