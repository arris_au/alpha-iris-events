@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' '.$dataType->getTranslatedAttribute('display_name_plural'))

@section('page_header')
  <div class="container-fluid">
    <h1 class="page-title">
      <i class="{{ $dataType->icon }}"></i> {{ $dataType->getTranslatedAttribute('display_name_plural') }}
    </h1>
    @can('add', app($dataType->model_name))
      <a
        href="{{ route('voyager.'.$dataType->slug.'.create') }}"
        class="btn btn-success btn-add-new"
      >
        <i class="voyager-plus"></i> <span>{{ __('voyager::generic.add_new') }}</span>
      </a>
    @endcan
    @foreach ($actions as $action)
      @if (method_exists($action, 'massAction'))
        @include('voyager::bread.partials.actions', ['action' => $action, 'data' => null])
      @endif
    @endforeach
    @include('voyager::multilingual.language-selector')
  </div>
@stop

@section('content')
  <div class="page-content browse container-fluid">
    @include('voyager::alerts')
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-bordered">
          <div class="panel-body">

            <livewire:alpha-iris-rsvp-table
              data-type="ticket-rsvps"
              per-page="10"
              can-search="true"
              exportable="true"
            />
          </div>
        </div>
      </div>
    </div>
  </div>

  {{-- Single delete modal --}}
  <div
    class="modal modal-danger fade"
    tabindex="-1"
    id="delete_modal"
    role="dialog"
  >
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button
            type="button"
            class="close"
            data-dismiss="modal"
            aria-label="{{ __('voyager::generic.close') }}"
          ><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"><i class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }}
            {{ strtolower($dataType->getTranslatedAttribute('display_name_singular')) }}?</h4>
        </div>
        <div class="modal-footer">
          <form
            action="#"
            id="delete_form"
            method="POST"
          >
            {{ method_field('DELETE') }}
            {{ csrf_field() }}
            <input
              type="submit"
              class="btn btn-danger pull-right delete-confirm"
              value="{{ __('voyager::generic.delete_confirm') }}"
            >
          </form>
          <button
            type="button"
            class="btn btn-default pull-right"
            data-dismiss="modal"
          >{{ __('voyager::generic.cancel') }}</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
@stop

@section('css')
  @if (! $dataType->server_side && config('dashboard.data_tables.responsive'))
    <link
      rel="stylesheet"
      href="{{ voyager_asset('lib/css/responsive.dataTables.min.css') }}"
    >
  @endif
@stop

@section('javascript')
  <script>
    $('.table-cell').on('click', '.delete', function(e) {
      $('#delete_form')[0].action = '{{ route('voyager.'.$dataType->slug.'.destroy', '__id') }}'.replace('__id', $(this).data('id'));
      $('#delete_modal').modal('show');
    });
  </script>
@stop
