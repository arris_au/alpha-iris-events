@extends('voyager::bread.edit-add')

@push('footerScripts')
    <script>
        $('document').ready(function () {
            var $eventSelect = $('select[name=event_id]');
            var $ticketSelect = $('select[name=ticket_id]');

            $ticketSelect.data('event_id', $eventSelect.val());

            $eventSelect.on('change', function () {
                $ticketSelect.prop("disabled", false);
                $ticketSelect.data('event_id', this.value);
                $ticketSelect.val(null).trigger('change');
            });

            $ticketSelect.on('change', function () {
                // Clear event fields
                tinymce.remove('.eav-field textarea.richTextBox');
                $('.eav-field').remove();

                if (!$ticketSelect.val()) {
                    return;
                }

                // Get ticket details/price
                $.ajax({
                    url: "{{ route('voyager.tickets.show', 'ticket_id') }}".replace('ticket_id', $ticketSelect.val()),
                    dataType: 'json',
                })
                .done(function (data) {
                    $('input[name="ticket_price"]').val(data.price);
                })
                .fail(function (error) {
                    console.log(error);
                });

                // Fetch event fields
                $.ajax({
                    url: "{{ route('voyager.event-fields.for-event-ticket', ['event_id', 'ticket_id']) }}"
                        .replace('event_id', $eventSelect.val())
                        .replace('ticket_id', $ticketSelect.val()),
                    dataType: 'json',
                })
                .done(function (data) {
                    data.forEach(function (field) {
                        $field = $(field.html).addClass('eav-field');
                        $('.page-content .panel-body').append($field);
                        if (field.type == 'rich_text_box') {
                            var additionalConfig = {
                                selector: 'textarea.richTextBox[name="'+field.field+'"]',
                            }
                            $.extend(additionalConfig, field.details.tinymceOptions || {});
                            tinymce.init(window.voyagerTinyMCE.getConfig(additionalConfig));
                        }
                    });
                })
                .fail(function (error) {
                    console.log(error);
                });
            });

            // Re-initialise ticket select2 with event_id ajax param
            $ticketSelect.select2('destroy').select2({
                width: '100%',
                tags: $ticketSelect.hasClass('taggable'),
                createTag: function (params) {
                    var term = $.trim(params.term);

                    if (term === '') {
                        return null;
                    }

                    return {
                        id: term,
                        text: term,
                        newTag: true
                    }
                },
                ajax: {
                    url: $ticketSelect.data('get-items-route'),
                    data: function (params) {
                        return {
                            search: params.term,
                            type: $ticketSelect.data('get-items-field'),
                            method: $ticketSelect.data('method'),
                            id: $ticketSelect.data('id'),
                            page: params.page || 1,
                            event_id: $ticketSelect.data("event_id")
                        };
                    }
                }
            });

            if (!$ticketSelect.val()) {
                $ticketSelect.prop("disabled", true);
            }
        })
    </script>
@endpush