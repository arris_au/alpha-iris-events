@extends('voyager-pages::layouts.default')
@section('meta_title', $event->name)
@section('page_title', $event->name)

@section('content')
    <div class="alpha-iris-margin-element event-detail-page">
        <h1>Thanks for your RSVP!</h1>
        <h2>You're coming to {{$event->name}}</h2>
        <div class="py-2 event-dates">
            {{ $event->event_start->format('F j @ g:i a')}} -  {{ $event->event_end->format('F j @ g:i a')}}
        </div>
        <div class="py-2 event-details">
            <a href="{{ route('events.show', $event) }}" class="btn btn-green">View Event Details</a>
        </div>

        <div class="py-2"></div>
    </div>
@stop
