@extends('voyager-pages::layouts.default')
@section('meta_title', $event->name)
@section('page_title', $event->name)

@section('content')
    <div class="alpha-iris-margin-element event-detail-page">
        <h1>{{ $event->name }}</h1>
        <div class="py-2 event-dates">
            {{ $event->event_start->format('F j @ g:i a') }} - {{ $event->event_end->format('F j @ g:i a') }}
        </div>

        {!! $event->description !!}

        <br>

        @if (count($errors) > 0)
            <div class="field-error">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if ($event->is_ticketed)
            <form method="POST" action="{{ route('events.cart', $event) }}">
                @csrf

                <div class="mb-5">
                    <div class="flex flex-wrap overflow-hidden tickets-table-headings border-b font-bold">
                        <div class="w-full overflow-hidden lg:w-3/5">
                            Ticket Type
                        </div>
                        <div class="w-1/2 overflow-hidden lg:w-1/5">
                            Price
                        </div>
                        <div class="w-1/2 overflow-hidden lg:w-1/5 text-center">
                            Quantity
                        </div>
                    </div>
                    @foreach ($event->tickets->sortBy('ticket_order') as $ticket)
                        @if ($ticket->should_display && $ticket->in_availablity_date)
                            <div class="flex flex-wrap overflow-hidden border-b py-2 tickets-table-row">
                                <div class="w-full overflow-hidden lg:w-3/5">
                                    <div class="ticket-name py-2">{{ $ticket->name }}</div>
                                    <div class="ticket-desc">{!! $ticket->description !!}</div>
                                    @if ($ticket->membership_types->count() > 0)
                                        <div class="ticket-requires">
                                            Membership required *
                                            @guest
                                                <div class="login">
                                                    <a href="{{ route('login') }}">Login</a>
                                                </div>
                                            @endguest
                                        </div>
                                    @endif
                                </div>
                                <div class="w-1/2 overflow-hidden lg:w-1/5">
                                    <div class="ticket-price py-2">{{ $ticket->display_price }}</div>
                                    <div class="ticket-quantity">
                                        {{ $ticket->qty_available }}
                                        @if (is_numeric($ticket->qty_available))
                                            available
                                        @endif
                                    </div>
                                </div>
                                <div class="w-1/2 overflow-hidden lg:w-1/5 text-center">
                                    @if ($ticket->qty_available === 0)
                                        <div class="ticket-sold-out">Sold Out</div>
                                    @else
                                        @if ($ticket->eligible)
                                            <button type="button"
                                                class="event-plus-button select-none w-6 inline-block align-middle"
                                                data-ticket-id="{{ $ticket->id }}"
                                                data-max-qty="{{ $ticket->qty_available }}">
                                                <svg viewbox="0 0 25 25">
                                                    <title>Plus</title>
                                                    <line x1="6" y1="12.5" x2="19" y2="12.5" fill="none"
                                                        stroke="currentColor" stroke-width="4" stroke-linecap="round"
                                                        vector-effect="non-scaling-stroke" />
                                                    <line y1="6" x1="12.5" y2="19" x2="12.5" fill="none"
                                                        stroke="currentColor" stroke-width="4" stroke-linecap="round"
                                                        vector-effect="non-scaling-stroke" />
                                                </svg>
                                            </button>
                                            <input type="number" class="w-14 ticket-qty text-center"
                                                id="ticket_{{ $ticket->id }}" name="ticket_{{ $ticket->id }}"
                                                value="0" step="1" min="0">
                                            <button type="button"
                                                class="event-minus-button w-6 select-none inline-block align-middle"
                                                data-ticket-id="{{ $ticket->id }}"
                                                data-max-qty="{{ $ticket->qty_available }}">
                                                <svg viewbox="0 0 25 25">
                                                    <title>Minus</title>
                                                    <line x1="6" y1="12.5" x2="19" y2="12.5" fill="none"
                                                        stroke="currentColor" stroke-width="4" stroke-linecap="round"
                                                        vector-effect="non-scaling-stroke" />
                                                </svg>
                                            </button>
                                        @else
                                            Membership Required
                                        @endif
                                    @endif
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
                <button class="btn btn-default my-5" type="submit">Register ></button>
            </form>
        @elseif ($event->needs_rsvp)
            <div class="mb-6 flex flex-wrap items-center">
                @include('alpha-iris-events::rsvp-actions')
            </div>
        @endif
    </div>
@stop

@push('footer-scripts')
    <script>
        var items = document.getElementsByClassName('event-plus-button');
        for (var i = 0; i < items.length; i++) {
            let ticketID = items[i].getAttribute('data-ticket-id');
            let ticketMax = items[i].getAttribute('data-max-qty');
            items[i].addEventListener('click', function() {
                aiIncrementTicketQty(ticketID, ticketMax);
            });
        }
        var items = document.getElementsByClassName('event-minus-button');
        for (var i = 0; i < items.length; i++) {
            let ticketID = items[i].getAttribute('data-ticket-id');
            let ticketMax = items[i].getAttribute('data-ticket-max');
            items[i].addEventListener('click', function() {
                aiDecrementTicketQty(ticketID, ticketMax);
            });
        }

        function aiIncrementTicketQty(ticketID, ticketMax) {
            let qtyEle = document.getElementById("ticket_" + ticketID);
            let currentQty = parseFloat(qtyEle.value);
            ticketMax = parseFloat(ticketMax);
            if (isNaN(ticketMax) || currentQty < ticketMax) {
                currentQty++;
            }
            qtyEle.value = currentQty;
        }

        function aiDecrementTicketQty(ticketID, ticketMax) {
            let qtyEle = document.getElementById("ticket_" + ticketID);
            let currentQty = parseFloat(qtyEle.value);
            if (currentQty > 0) {
                currentQty--;
            }
            qtyEle.value = currentQty;
        }
    </script>
@endpush
