@extends('voyager-pages::layouts.default')
@section('meta_title', $event->name)
@section('page_title', $event->name)

@section('content')
    <div class="alpha-iris-margin-element event-cart-page">
        <h1>Registration Details</h1>
        <div class="py-2 event-dates">
            {{ $event->name }} <br>
            {{ $event->event_start->format('F j @ g:i a') }} - {{ $event->event_end->format('F j @ g:i a') }}
        </div>
        <div class="py-2 event-details">
            <a href="{{ route('events.show', $event) }}">View Event Details</a>
        </div>
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form method="POST" action="{{ route('events.register', $event) }}">
            @csrf
            <div class="flex flex-wrap overflow-hidden border-b font-bold tickets-table-headings">
                <div class="w-full overflow-hidden lg:w-3/5">
                    Ticket Type Selected
                </div>
                <div class="w-1/2 overflow-hidden lg:w-1/5">
                    Price
                </div>
                <div class="w-1/2 overflow-hidden lg:w-1/5">
                    Quantity
                </div>
            </div>
            @foreach ($items as $item)
                <div class="flex flex-wrap overflow-hidden border-b py-2 tickets-table-row">
                    <div class="w-full overflow-hidden lg:w-3/5">
                        {{ $item['name'] }}
                    </div>
                    <div class="w-1/2 overflow-hidden lg:w-1/5">
                        {{ $item['display_price'] }}
                    </div>
                    <div class="w-1/2 overflow-hidden lg:w-1/5">
                        {{ $item['quantity'] }}
                    </div>
                </div>
            @endforeach
            <div class="flex flex-wrap overflow-hidden  py-2 tickets-table-footer-row">
                <div class="w-full overflow-hidden lg:w-3/5">&nbsp;</div>
                <div class="w-1/2 overflow-hidden lg:w-1/5 border-b font-bold event-cart-total">
                    Total
                </div>
                <div class="w-1/2 overflow-hidden lg:w-1/5 border-b font-bold">
                    {{ $displayTotal }}
                </div>
            </div>

            <h2>Registrant Details</h2>

            @include('alpha-iris::partials.two-col-fields', [
                'fields' => $fields,
            ])

            <h2>Payment</h2>
            @include('alpha-iris-payments::payment_selector')
            <button class="btn btn-default my-5" type="submit">Register ></button>
        </form>
    </div>
@stop

@push('footer-scripts')
@endpush
