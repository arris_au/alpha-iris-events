@extends('voyager-pages::layouts.default')
@section('meta_title', 'Events')
@section('page_title', 'Events')

@section('content')
    <div class="alpha-iris-margin-element events-page">
        @if (count($errors) > 0)
            <div class="field-error">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @forelse ($events as $event)
            <h2>{{ $event->name }}</h2>
            <p class="italic m-0">{{ $event->tags->pluck('name')->join(', ') }}</p>
            <p>
                {!! $event->description !!}
            </p>

            <div class="mb-6 flex flex-wrap items-center">
                @if ($event->is_ticketed)
                    <a href="{{ route('events.show', $event) }}" class="btn btn-default mb-2 mr-2">Buy Tickets</a>
                @else
                    <a href="{{ route('events.show', $event) }}" class="btn btn-green mb-2 mr-2">More Information</a>
                    @if ($event->needs_rsvp)
                        @include('alpha-iris-events::rsvp-actions')
                    @endif
                @endif
            </div>
        @empty
            <h2>Hmmm, there aren't any events coming up</h2>
            <p>Sorry, it looks like we don't have any upcoming events scheduled right now.</p>
            <p>Please check back later!</p>
        @endforelse
    </div>
@stop
