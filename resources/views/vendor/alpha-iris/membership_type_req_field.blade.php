@if ($action == 'read')
    <div>
        @if ($dataTypeContent->membership_types->isEmpty() && !$dataTypeContent->allow_any_member)
            <div class="label label-default">No membership requirement</div>
        @elseif ($dataTypeContent->allow_any_member)
            <div class="label label-default">Require any valid membership</div>
        @else
            @foreach ($dataTypeContent->membership_types as $type)
                <div class="label label-default">{{ $type->name }}</div>
            @endforeach
        @endif
    </div>
@else
    <div>
        <select
            name="membership_type_req_select"
            id="membership_type_req_select"
            multiple
            size="5"
            class="form-control select2"
            onChange="updateMembershipTypes()"
        >
            <option
                value="0"
                @if ($dataTypeContent->membership_types->isEmpty() && !$dataTypeContent->allow_any_member) selected @endif
            >No membership requirement</option>
            <option
                value="-1"
                @if ($dataTypeContent->allow_any_member) selected @endif
            >Require any valid membership</option>
            @foreach (\AlphaIris\Core\Models\MembershipType::all() as $type)
                <option
                    value="{{ $type->id }}"
                    @if ($dataTypeContent->membership_types->contains($type->id)) selected @endif
                >{{ $type->name }}</option>
            @endforeach
        </select>
        <input
            type="hidden"
            name="membership_type_req"
            id="membership_type_req"
            value="{{ $dataTypeContent->membership_type_req }}"
        />
        <input
            type="hidden"
            name="allow_any_member"
            id="allow_any_member"
            value="{{ $dataTypeContent->allow_any_member }}"
        />
    </div>

    @push('javascript')
        <script>
            var membershipTypeReqSelected = [];

            $('document').ready(function () {
                membershipTypeReqSelected = $('#membership_type_req_select').val();
            });

            /**
             * Handle updates to the membership types field.
             */
            function updateMembershipTypes() {
                $select = $('#membership_type_req_select');
                let selected = $select.val();

                if (JSON.stringify(membershipTypeReqSelected) == JSON.stringify(selected)) {
                    return;
                }

                // "No membership requirement" can't be selected with any other options.
                let nonePrevSelected = membershipTypeReqSelected.indexOf('0') > -1;
                let noneSelected = selected.indexOf('0') > -1;
                if (nonePrevSelected && noneSelected) {
                    selected.splice(selected.indexOf('0'), 1);
                } else if (noneSelected) {
                    selected = ['0'];
                }

                // "Require any valid membership" can't be selected with any other options.
                let anyPrevSelected = membershipTypeReqSelected.indexOf('-1') > -1;
                let anySelected = selected.indexOf('-1') > -1;
                if (anyPrevSelected && anySelected) {
                    selected.splice(selected.indexOf('-1'), 1);
                } else if (anySelected) {
                    selected = ['-1'];
                }

                // Update hidden fields
                $('#membership_type_req').val(JSON.stringify(selected));
                if (selected.indexOf('-1') > -1) {
                    $('#allow_any_member').val('1');
                } else {
                    $('#allow_any_member').val('0');
                }

                membershipTypeReqSelected = selected;
                $select.val(selected).trigger('change');
            }
        </script>
    @endpush
@endif
