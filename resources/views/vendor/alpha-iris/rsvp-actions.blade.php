@if ($event->userSentRsvp())
    <div class="p-3 bg-blue-200 text-blue-700 rounded-lg mb-2 mr-2">You have sent an RSVP for this event</div>
@elseif ($event->rsvp_available)
    <a class="btn btn-default mb-2 mr-2" href="{{ route('events.rsvp', $event) }}">RSVP</a>
    <span class="mb-2 mr-2">{{ $event->rsvp_qty_available }} available</span>
@else
    <div class="p-3 bg-blue-200 text-blue-700 rounded-lg mb-2 mr-2">The maximum limit has been reached for this event</div>
@endif