@extends('voyager-pages::layouts.default')
@section('meta_title', $event->name)
@section('page_title', $event->name)

@section('content')
    <div class="alpha-iris-margin-element event-detail-page">
        <h1>RSVP for {{ $event->name }}</h1>
        <div class="event-dates py-2">
            {{ $event->event_start->format('F j @ g:i a') }} - {{ $event->event_end->format('F j @ g:i a') }}
        </div>
        <div class="event-details py-2">
            <a href="{{ route('events.show', $event) }}" class="btn btn-green">
                View Event Details
            </a>
        </div>
        @if (count($errors) > 0)
            <div class="field-error">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form method="POST" action="{{ route('events.sendrsvp', $event) }}">
            @csrf

            <h2>Your Details</h2>

            @include('alpha-iris::partials.two-col-fields', [
                'fields' => $fields,
            ])

            <div class="py-2"></div>

            <button class="btn btn-default" type="submit">Send RSVP</button>
        </form>
        <div class="py-2"></div>
    </div>
@stop
