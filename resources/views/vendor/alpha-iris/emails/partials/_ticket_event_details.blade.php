<h2>Event</h2>
<table class="min-w-full">
    <tbody>
        <tr class="border-b border-t">
            <td valign="top" class="p-2 text-left font-bold w-1/5">When</td>
            <td valign="top" class="p-2 text-left">
                {{ $ticket->event->event_start->format('F j @ g:i a')}} - {{ $ticket->event->event_end->format('F j @ g:i a')}}
            </td>
        </tr>
        <tr class="border-b">
            <td valign="top" class="p-2 text-left font-bold w-1/5">Where</td>
            <td valign="top" class="p-2 text-left">
                {!! $ticket->event->venue->html_formatted_address !!}
            </td>
        </tr>
    </tbody>
</table>