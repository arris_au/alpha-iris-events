<h2>Your Details</h2>
<table class="min-w-full">
    <tbody>
        <tr class="border-b border-t">
            <td valign="top" class="p-2 text-left font-bold w-1/5">Name</td>
            <td valign="top" class="p-2 text-left">{{ $ticket->first_name }} {{ $ticket->last_name }}</td>
        </tr>
        <tr class="border-b">
            <td valign="top" class="p-2 text-left font-bold w-1/5">Email</td>
            <td valign="top" class="p-2 text-left">{{ $ticket->email }}</td>
        </tr>
        <tr class="border-b">
            <td valign="top" class="p-2 text-left font-bold w-1/5">Phone</td>
            <td valign="top" class="p-2 text-left">{{ $ticket->phone }}</td>
        </tr>
        @foreach ($ticket->eav_attributes as $attribute)
            @if ($attribute->browse)
                <tr class="border-b">
                    <td valign="top" class="p-2 text-left font-bold w-1/5">
                        {{ $attribute->display_name }}
                    </td>
                    <td valign="top" class="p-2 text-left">
                        @include('alpha-iris::partials._render_attribute', [
                            'dataTypeContent' => $ticket,
                            'row' => $attribute,
                        ])
                    </td>
                </tr>
            @endif
        @endforeach
        <tr class="border-b">
            <td valign="top" class="p-2 text-left font-bold w-1/5">Address</td>
            <td valign="top" class="p-2 text-left">{!! $ticket->html_formatted_address !!}</td>
        </tr>
    </tbody>
</table>
