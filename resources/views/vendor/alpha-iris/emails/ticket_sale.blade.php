@extends('alpha-iris::email.template')
@section('content')
    <p>Dear {{ $ticketSale->first_name }},</p>

    <p>Thanks for buying a ticket to {{ $ticketSale->event->name }}!</p>

    <p>Here's some information about your purchase</p>
    @include(
        'alpha-iris-events::emails.partials._ticket_event_details',
        ['ticket' => $ticketSale]
    )

    <h2>Ticket Details</h2>
    <table class="min-w-full">
        <tbody>
            <tr class="border-b border-t">
                <td valign="top" class="p-2 text-left font-bold w-1/5">Ticket Type</td>
                <td valign="top" class="p-2 text-left">{{ $ticketSale->ticket->name }}</td>
            </tr>
            <tr class="border-b">
                <td valign="top" class="p-2 text-left font-bold w-1/5">Cost</td>
                <td valign="top" class="p-2 text-left">${{ AlphaIris\Payments\Services\Payments::priceIncTax($ticketSale->ticket->price) }}</td>
            </tr>
        </tbody>
    </table>

    @include(
        'alpha-iris-events::emails.partials._ticket_your_details',
        ['ticket' => $ticketSale]
    )
@endsection
