@extends('alpha-iris::email.template')
@section('content')

<p>Dear {{ $ticketRsvp->first_name }},</p>

<p>Thanks for your RSVP to {{ $ticketRsvp->event->name}}!</p>

@include('alpha-iris-events::emails.partials._ticket_event_details', ['ticket' => $ticketRsvp])

@include('alpha-iris-events::emails.partials._ticket_your_details', ['ticket' => $ticketRsvp])

@endsection

