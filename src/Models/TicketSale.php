<?php

namespace AlphaIris\Events\Models;

use AlphaIris\Core\Models\Country;
use AlphaIris\Core\Models\State;
use AlphaIris\Core\Services\AlphaIris;
use AlphaIris\Core\Traits\HasEav;
use AlphaIris\Events\Events\EventCheckIn;
use AlphaIris\Shopping\Models\OrderItem;
use Illuminate\Database\Eloquent\Model;

class TicketSale extends Model
{
    use HasEav {
        loadEAValues as protectedLoadEAValues;
    }

    protected $fillable = [
        'event_id',
        'ticket_id',
        'user_id',
        'ticket_price',
        'order_payment_status',
        'first_name',
        'last_name',
        'address_1',
        'address_2',
        'suburb',
        'state_id',
        'postcode',
        'country_id',
    ];

    protected $dates = [
        'updated_at',
        'created_at',
        'checked_in',
    ];

    public function __construct(array $attributes = [])
    {
        $this->attributeClass = EventField::class;
        $this->attributeValueClass = EventFieldValue::class;
        parent::__construct($attributes);
    }

    protected function loadEAValues()
    {
        $this->eavAttributes = null;
        $this->protectedLoadEAValues();
    }

    public function setAttribute($key, $value)
    {
        parent::setAttribute($key, $value);
        if ($key == 'event_id') {
            $this->eavAttributes = null;
        }
    }

    public function eav_attributes()
    {
        return $this->hasMany(EventField::class, 'event_id', 'event_id')
            ->whereIn('id', function ($q) {
                $q->select('event_field_id')
                    ->from('event_field_ticket')
                    ->where('ticket_id', $this->ticket_id);
            });
    }

    public function ticket()
    {
        return $this->belongsTo(Ticket::class);
    }

    public function event()
    {
        return $this->belongsTo(Event::class);
    }

    public function state()
    {
        return $this->belongsTo(State::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function getHtmlFormattedAddressAttribute()
    {
        $address = AlphaIris::formatAddress(
            $this->address_1,
            $this->address_2,
            $this->suburb,
            $this->postcode,
            $this->state,
            $this->country
        );

        return str_replace("\n", '<br>', $address);
    }

    /**
     * Does this model have an EAV attribute of the specified name?
     *
     * @param [type] $key
     *
     * @return bool
     */
    public function hasAttribute($key)
    {
        if (in_array($key, ['eav_attributes', 'eav_model_name', 'event_id', 'ticket_id'])) {
            return false;
        }

        return $this->getEavAttributeModel($key) ? true : false;
    }

    public function orderItem()
    {
        return $this->belongsTo(OrderItem::class, 'id', 'model_id')->where('model_class', static::class);
    }

    public function getItemTypeAttribute()
    {
        return 'sale';
    }

    protected static function boot()
    {
        parent::boot();
        self::saving(function ($model) {
            if (is_null($model->getOriginal('checked_in')) && ! is_null($model->checked_in)) {
                EventCheckIn::dispatch($model);
            }
        });
    }
}
