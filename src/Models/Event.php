<?php

namespace AlphaIris\Events\Models;

use AlphaIris\Core\Models\Tag;
use AlphaIris\Core\Traits\HasEav;
use AlphaIris\Core\Traits\HasUUID;
use AlphaIris\Core\Traits\RoutesUUID;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use HasUUID, RoutesUUID, HasEav;

    protected $dates = [
        'event_start',
        'event_end',
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'name',
        'description',
        'venue_id',
        'event_start',
        'event_end',
        'is_ticketed',
        'needs_rsvp',
        'rsvp_limit',
        'is_private',
    ];

    public function event_fields()
    {
        return $this->hasMany(EventField::class);
    }

    public function tickets()
    {
        return $this->hasMany(Ticket::class);
    }

    public function venue()
    {
        return $this->belongsTo(Venue::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'event_tags');
    }

    /**
     * Scope a query to only include RSVP events.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeRsvp($query)
    {
        return $query->where('needs_rsvp', true);
    }

    /**
     * Scope a query to only include ticketed events.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeTicketed($query)
    {
        return $query->where('is_ticketed', true);
    }

    public function getRsvpQtyAvailableAttribute()
    {
        if ($this->rsvp_limit == 0) {
            return 'Unlimited';
        }

        $rsvps = TicketRsvp::where('event_id', $this->id)->count();

        return $this->rsvp_limit - $rsvps;
    }

    public function getRsvpAvailableAttribute()
    {
        if ($this->rsvp_limit == 0) {
            return true;
        }

        return $this->rsvp_qty_available > 0;
    }

    public function userSentRsvp($user = null)
    {
        if (is_null($user)) {
            $user = auth()->user();
        }
        if (is_null($user)) {
            return false;
        }

        return TicketRsvp::where('event_id', $this->id)->where('user_id', $user->id)->count() > 0;
    }
}
