<?php

namespace AlphaIris\Events\Models;

use Illuminate\Database\Eloquent\Model;

class EventFieldValue extends Model
{
    protected $table = 'event_values';

    protected $fillable = [
        'attribute_id',
        'model_id',
        'string_value',
        'text_value',
        'dt_value',
        'float_value',
        'bool_value',
    ];

    public function attribute()
    {
        try {
            return $this->belongsTo(EventField::class, 'attribute_id', 'id');
        } catch (\Exception $e) {
            return;
        }
    }

    public function setTransposedValueAttribute($value)
    {
        switch ($this->attribute->type) {
            case 'checkbox':
                $value = (in_array($value, [true, 1, '1', 'on', 'checked'], true));
                $this->bool_value = $value;
                break;
            case 'date':
            case 'time':
            case 'timestamp':
                $this->dt_value = $value;
                break;
            case 'rich_text_box':
            case 'code_editor':
            case 'markdown_editor':
            case 'text_area':
            case 'laraberg':
                $this->text_value = $value;
                break;
            case 'number':
                $this->float_value = $value;
                break;
            case 'multiple_checkbox':
            case 'select_multiple':
                $this->string_value = json_encode($value);
                break;
            case 'frontend_layout':
            case 'file':
            case 'multiple_images':
            case 'image':
            case 'media_picker':
            case 'password':
            case 'radio_btn':
                throw new \Exception('Unsupported type '.$this->attribute->type);
            case 'text':
            case 'select_dropdown':
            case 'hidden':
            case 'coordinates':
                $this->string_value = $value;
                break;
        }
    }

    public function getTransposedValueAttribute()
    {
        switch ($this->attribute->type) {
            case 'checkbox':
                return $this->bool_value;
            case 'date':
            case 'time':
            case 'timestamp':
                return $this->dt_value;
            case 'rich_text_box':
            case 'code_editor':
            case 'markdown_editor':
            case 'text_area':
            case 'laraberg':
                return $this->text_value;
            case 'number':
                return $this->float_value;
            case 'multiple_checkbox':
            case 'select_multiple':
                return $this->string_value;
            case 'frontend_layout':
            case 'file':
            case 'multiple_images':
            case 'image':
            case 'media_picker':
            case 'password':
            case 'radio_btn':
                throw new \Exception('Unsupported type '.$this->attribute->type);
            case 'text':
            case 'select_dropdown':
            case 'hidden':
            case 'coordinates':
                return $this->string_value;
        }
    }
}
