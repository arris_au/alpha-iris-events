<?php

namespace AlphaIris\Events\Models;

use AlphaIris\Core\Models\Country;
use AlphaIris\Core\Models\State;
use AlphaIris\Core\Services\AlphaIris;
use Illuminate\Database\Eloquent\Model;

class Venue extends Model
{
    protected $fillable = [
        'name',
        'description',
        'address_1',
        'address_2',
        'suburb',
        'state_id',
        'postcode',
        'country_id',
    ];

    public function state()
    {
        return $this->belongsTo(State::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function getHtmlFormattedAddressAttribute()
    {
        $address = AlphaIris::formatAddress(
            $this->address_1,
            $this->address_2,
            $this->suburb,
            $this->postcode,
            $this->state,
            $this->country
        );

        return str_replace("\n", '<br>', $address);
    }
}
