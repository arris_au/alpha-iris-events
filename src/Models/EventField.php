<?php

namespace AlphaIris\Events\Models;

use Illuminate\Database\Eloquent\Model;

class EventField extends Model
{
    protected $fillable = [
        'event_id',
        'field',
        'type',
        'display_name',
        'required',
        'browse',
        'read',
        'edit',
        'add',
        'delete',
        'details',
        'order',
    ];

    public function event()
    {
        return $this->belongsTo(Event::class);
    }

    public function tickets()
    {
        return $this->belongsToMany(Ticket::class);
    }

    public function getTranslatedAttribute($key)
    {
        return $this->$key;
    }

    public function getDetailsAttribute($value)
    {
        return json_decode(! empty($value) ? $value : '{}');
    }

    public function setDetailsAttribute($value)
    {
        if (is_object($value)) {
            $this->attributes['details'] = json_encode($value);
        } else {
            $this->attributes['details'] = $value;
        }
    }

    /**
     * Scope a query to only include event fields for the specified event.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeForEvent($query, $event_id)
    {
        return $query->where('event_id', $event_id);
    }

    /**
     * Scope a query to only include event fields for the specified ticket.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeForTicket($query, $ticket_id)
    {
        return $query->whereHas('tickets', fn ($q) => $q->where('id', $ticket_id));
    }
}
