<?php

namespace AlphaIris\Events\Models;

use AlphaIris\Core\Models\MembershipType;
use AlphaIris\Core\Traits\HasEav;
use AlphaIris\Payments\Services\Payments;
use Freshbitsweb\LaravelCartManager\Traits\Cartable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Ticket extends Model
{
    use Cartable, HasEav;

    protected $fillable = [
        'name',
        'description',
        'event_id',
        'price',
        'membership_type_req',
        'limit',
        'available_from',
        'available_to',
        'ticket_order',
        'allow_any_member',
        'hide_unauth',
    ];

    public function event()
    {
        return $this->belongsTo(Event::class);
    }

    public function getShouldDisplayAttribute()
    {
        if (! $this->hide_unauth) {
            return true;
        }

        if ($this->allow_any_member && ! Auth::guest()) {
            return true;
        }
        $types = json_decode($this->membership_type_req);
        if (is_null($types) || count($types) == 0) {
            return true;
        }

        return $this->eligible;
    }

    public function getInAvailablityDateAttribute(): bool
    {
        $now = now();

        // Check if both available_from and available_to are set
        if ($this->available_from && $this->available_to) {
            return $now->gte($this->available_from) && $now->lte($this->available_to);
        }

        // Check if only available_from is set
        if ($this->available_from) {
            return $now->gte($this->available_from);
        }

        // Check if only available_to is set
        if ($this->available_to) {
            return $now->lte($this->available_to);
        }

        // Display the ticket if neither available_from nor available_to is set
        return true;
    }

    public function getMembershipTypesAttribute()
    {
        $types = json_decode($this->membership_type_req);
        if (is_array($types)) {
            return MembershipType::whereIn('id', $types)->get();
        } else {
            return collect();
        }
    }

    public function getQtyAvailableAttribute()
    {
        if ($this->limit == 0) {
            return 'Unlimited';
        } else {
            $items = TicketSale::where('ticket_id', $this->id)->count();

            return $this->limit - $items;
        }
    }

    public function qtyAvailable($required)
    {
        if ($this->limit == 0) {
            return true;
        } else {
            return $required <= $this->qty_available;
        }
    }

    public function getTaxAttribute()
    {
        return round(($this->price * config('cart_manager.tax_percentage')) / 100, 2);
        /**$country = Auth::check() ? auth()->user()->country : setting('payment.store_country');
        $state = Auth::check() ? auth()->user()->state : setting('payment.store_state');
        $tax = 0;
        foreach (TaxRule::all() as $rule) {
            if ($rule->applicable($country, $state)) {
                $tax += $rule->calculate($this->price);
            }
        }

        return $tax;**/
    }

    public function getDisplayPriceAttribute()
    {
        return Payments::formatCurrency($this->price + $this->tax);
    }

    public function getEligibleAttribute()
    {
        $mTypes = $this->membership_types;
        if ($mTypes->count() == 0 && ! $this->allow_any_member) {
            return true;
        }

        if (! auth()->user()) {
            return false;
        }

        if ($this->allow_any_member) {
            return true;
        }

        $mType = optional(auth()->user()->membership)->membership_type;
        if (! $mType) {
            return false;
        }

        return $mTypes->contains($mType->id);
    }
}
