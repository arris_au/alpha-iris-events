<?php

namespace AlphaIris\Events\Events;

use AlphaIris\Events\Models\Event;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class EventCheckIn
{
    use Dispatchable;
    use InteractsWithSockets;
    use SerializesModels;

    public $ticketSaleRSVP;
    public $event;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($ticketSaleRSVP)
    {
        $this->event = $ticketSaleRSVP->event;
        $this->ticketSaleRSVP = $ticketSaleRSVP;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
