<?php

namespace AlphaIris\Events\Actions;

use TCG\Voyager\Actions\AbstractAction;

class TicketSalesAction extends AbstractAction
{
    public function getTitle()
    {
        return ($this->data->is_ticketed) ? 'Ticket Sales' : 'RSVPs';
    }

    public function getIcon()
    {
    }

    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-primary pull-left',
        ];
    }

    public function shouldActionDisplayOnDataType()
    {
        return $this->dataType->slug == 'tickets';
    }

    public function getDefaultRoute()
    {
        if ($this->data->is_ticketed) {
            return route('');
        } else {
        }
    }
}
