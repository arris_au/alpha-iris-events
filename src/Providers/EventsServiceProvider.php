<?php

namespace AlphaIris\Events\Providers;

use AlphaIris\Core\Services\PublicFieldsService;
use AlphaIris\Events\Http\Livewire\AdminTicketRsvpTable;
use AlphaIris\Events\Http\Livewire\AdminTicketSaleTable;
use AlphaIris\Events\Http\Livewire\AdminTicketsTable;
use AlphaIris\Events\Models\TicketRsvp;
use AlphaIris\Events\Models\TicketSale;
use AlphaIris\Events\Observers\TicketRsvpObserver;
use AlphaIris\Events\Observers\TicketSaleObserver;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;
use Livewire\Livewire;
use TCG\Voyager\Models\DataType;

class EventsServiceProvider extends ServiceProvider
{
    public function boot()
    {
        Livewire::component('alpha-iris-ticket-table', AdminTicketsTable::class);
        Livewire::component('alpha-iris-rsvp-table', AdminTicketRsvpTable::class);
        Livewire::component('alpha-iris-ticket-sales-table', AdminTicketSaleTable::class);

        $this->loadMigrationsFrom(__DIR__.'/../../database/migrations');

        $this->loadRoutesFrom(__DIR__.'../../../routes/web.php');

        $ticketSaleModel = DataType::where('name', 'ticket_sales')->first();
        if ($ticketSaleModel) {
            $fields = [
                'first_name',
                'last_name',
                'company',
                'email',
                'phone',
                'address_1',
                'address_2',
                'suburb',
                'ticket_sale_belongsto_state_relationship',
                'postcode',
                'ticket_sale_belongsto_country_relationship',
            ];

            $rows = $ticketSaleModel->rows()->whereIn('field', $fields)->get();

            PublicFieldsService::add($ticketSaleModel, $rows);
        }

        $ticketRSVPModel = DataType::where('name', 'ticket_rsvps')->first();
        if ($ticketRSVPModel) {
            $fields = [
                'first_name',
                'last_name',
                'company',
                'email',
                'phone',
                'address_1',
                'address_2',
                'suburb',
                'ticket_rsvp_belongsto_state_relationship',
                'postcode',
                'ticket_rsvp_belongsto_country_relationship',
            ];

            $rows = $ticketRSVPModel->rows()->whereIn('field', $fields)->get();

            PublicFieldsService::add($ticketRSVPModel, $rows);
        }
        TicketSale::observe(TicketSaleObserver::class);
        TicketRsvp::observe(TicketRsvpObserver::class);
    }

    public function register()
    {
        $this->loadViewsFrom([
            App::basePath().'/resources/views/alpha-iris-events',
            __DIR__.'/../../resources/views/',
            __DIR__.'/../../resources/views/vendor/alpha-iris',
        ], 'alpha-iris-events');

        $this->loadViewsFrom(__DIR__.'/../../resources/views/vendor/voyager', 'voyager');
    }
}
