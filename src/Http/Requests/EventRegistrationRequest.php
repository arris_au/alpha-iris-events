<?php

namespace AlphaIris\Events\Http\Requests;

use AlphaIris\Core\Services\PublicFieldsService;
use AlphaIris\Events\Models\EventField;
use AlphaIris\Events\Models\Ticket;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Route;
use TCG\Voyager\Models\DataType;

class EventRegistrationRequest extends FormRequest
{
    protected $fields;
    protected $event;

    public function __construct()
    {
        $this->event = Route::current()->parameters['event'];
        $this->fields = $this->event->needs_rsvp
            ? $this->getRSVPPublicFields($this->event->id)
            : $this->getPublicFields($this->event->id);

        parent::__construct();
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        foreach ($this->fields as $field) {
            if (property_exists($field->details, 'validation')) {
                if (property_exists($field->details->validation, 'rule')) {
                    $fieldName = $field->field;
                    if ($field->type == 'relationship') {
                        $fieldName = $field->details->column;
                    }
                    $rules[$fieldName] = $field->details->validation->rule;
                }
            }
        }

        return $rules;
    }

    public function messages()
    {
        $messages = [];
        foreach ($this->fields as $field) {
            if (property_exists($field->details, 'validation')) {
                if (property_exists($field->details->validation, 'messages')) {
                    $fieldName = $field->field;
                    if ($field->type == 'relationship') {
                        $fieldName = $field->details->column;
                    }
                    foreach ($field->details->validation->messages as $rule => $message) {
                        $messages[$fieldName.'.'.$rule] = $message;
                    }
                }
            }
        }

        return $messages;
    }

    public function attributes()
    {
        $attributes = [];
        foreach ($this->fields as $field) {
            if (property_exists($field->details, 'validation')) {
                $fieldName = $field->field;
                if ($field->type == 'relationship') {
                    $fieldName = $field->details->column;
                }
                // Show the field's display name on the error message
                if (! empty($field->display_name)) {
                    if (! empty($data[$fieldName]) && is_array($data[$fieldName])) {
                        foreach ($data[$fieldName] as $index => $element) {
                            if ($element instanceof UploadedFile) {
                                $name = $element->getClientOriginalName();
                            } else {
                                $name = $index + 1;
                            }

                            $attributes[$fieldName.'.'.$index] = $field->getTranslatedAttribute('display_name').' '.$name;
                        }
                    } else {
                        $attributes[$fieldName] = $field->getTranslatedAttribute('display_name');
                    }
                }
            }
        }

        return $attributes;
    }

    protected function getRSVPPublicFields($eventId)
    {
        $ticketsDT = DataType::where('name', 'ticket_rsvps')->first();
        $allFields = PublicFieldsService::fields($ticketsDT);

        $eventFields = EventField::where('event_id', $eventId)->get();

        $allFields = $allFields->merge($eventFields);

        return $allFields;
    }

    protected function getPublicFields($eventId)
    {
        $ticketsDT = DataType::where('name', 'ticket_sales')->first();
        $allFields = PublicFieldsService::fields($ticketsDT);
        $eventFields = EventField::where('event_id', $eventId)
            ->whereHas('tickets', function ($q) use ($eventId) {
                $q->whereIn('ticket_id', $this->getCartTickets($eventId)->pluck('modelId')->toArray());
            })
            ->get();

        $allFields = $allFields->merge($eventFields);

        return $allFields;
    }

    public function getCartTickets($eventId)
    {
        $allItems = cart()->items();
        $items = collect();
        foreach ($allItems as $item) {
            if ($item['modelType'] == Ticket::class) {
                $ticket = Ticket::find($item['modelId']);
                if ($ticket && $ticket->event->id == $eventId) {
                    $items->add($item);
                }
            }
        }

        return $items;
    }
}
