<?php

namespace AlphaIris\Events\Http\Controllers;

use AlphaIris\Core\Http\Controllers\AlphaIrisBreadController;
use AlphaIris\Events\Models\EventField;
use AlphaIris\Events\Models\Ticket;
use AlphaIris\Events\Models\TicketSale;
use Illuminate\Http\Request;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Facades\Voyager;

class TicketSaleController extends AlphaIrisBreadController
{
    /**
     * Store data. Customised POST BRE(A)D - Expects input for relevant event fields.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        // Add event fields to data type rows
        $rows = $dataType->addRows->merge(
            EventField::forEvent($request->input('event_id'))
                ->forTicket($request->input('ticket_id'))
                ->get()
        );

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $rows)->validate();
        $data = $this->insertUpdateData($request, $slug, $rows, new $dataType->model_name());

        event(new BreadDataAdded($dataType, $data));

        if (! $request->has('_tagging')) {
            if (auth()->user()->can('browse', $data)) {
                $redirect = redirect()->route("voyager.{$dataType->slug}.index");
            } else {
                $redirect = redirect()->back();
            }

            return $redirect->with([
                'message'    => __('voyager::generic.successfully_added_new')." {$dataType->getTranslatedAttribute('display_name_singular')}",
                'alert-type' => 'success',
            ]);
        } else {
            return response()->json(['success' => true, 'data' => $data]);
        }
    }

    public function insertUpdateData($request, $slug, $rows, $data)
    {
        $result = parent::insertUpdateData($request, $slug, $rows, $data);

        foreach ($result->eav_attributes as $eav) {
            $result->{$eav->field} = $request->get($eav->field);
        }
        $result->save();

        return $result;
    }

    public function toggleCheckin(Request $request, TicketSale $ticketSale)
    {
        $ticketSale->checked_in 
            ? $ticketSale->checked_in = null
            : $ticketSale->checked_in = now();

        $ticketSale->save();
    }

    /**
     * Get BREAD relations data.
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function relation(Request $request)
    {
        $slug = $this->getSlug($request);
        $page = $request->input('page');
        $on_page = 50;
        $search = $request->input('search', false);
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $method = $request->input('method', 'add');

        $model = app($dataType->model_name);
        if ($method != 'add') {
            $model = $model->find($request->input('id'));
        }

        $this->authorize($method, $model);

        $rows = $dataType->{$method.'Rows'};
        foreach ($rows as $key => $row) {
            if ($row->field === $request->input('type')) {
                $options = $row->details;
                $model = app($options->model);
                $skip = $on_page * ($page - 1);

                $additional_attributes = $model->additional_attributes ?? [];

                // Apply local scope if it is defined in the relationship-options
                if (isset($options->scope) && $options->scope != '' && method_exists($model, 'scope'.ucfirst($options->scope))) {
                    $model = $model->{$options->scope}();
                }

                // Filter by the selected event
                if ($request->input('event_id')) {
                    $model = $model->where('event_id', $request->input('event_id'));
                }

                // If search query, use LIKE to filter results depending on field label
                if ($search) {
                    // If we are using additional_attribute as label
                    if (in_array($options->label, $additional_attributes)) {
                        $relationshipOptions = $model->get();
                        $relationshipOptions = $relationshipOptions->filter(function ($model) use ($search, $options) {
                            return stripos($model->{$options->label}, $search) !== false;
                        });
                        $total_count = $relationshipOptions->count();
                        $relationshipOptions = $relationshipOptions->forPage($page, $on_page);
                    } else {
                        $total_count = $model->where($options->label, 'LIKE', '%'.$search.'%')->count();
                        $relationshipOptions = $model->take($on_page)->skip($skip)
                            ->where($options->label, 'LIKE', '%'.$search.'%')
                            ->get();
                    }
                } else {
                    $total_count = $model->count();
                    $relationshipOptions = $model->take($on_page)->skip($skip)->get();
                }

                $results = [];

                if (! $row->required && ! $search && $page == 1) {
                    $results[] = [
                        'id' => '',
                        'text' => __('voyager::generic.none'),
                    ];
                }

                // Sort results
                if (! empty($options->sort->field)) {
                    if (! empty($options->sort->direction) && strtolower($options->sort->direction) == 'desc') {
                        $relationshipOptions = $relationshipOptions->sortByDesc($options->sort->field);
                    } else {
                        $relationshipOptions = $relationshipOptions->sortBy($options->sort->field);
                    }
                }

                foreach ($relationshipOptions as $relationshipOption) {
                    $results[] = [
                        'id' => $relationshipOption->{$options->key},
                        'text' => $relationshipOption->{$options->label},
                    ];
                }

                return response()->json([
                    'results' => $results,
                    'pagination' => [
                        'more' => ($total_count > ($skip + $on_page)),
                    ],
                ]);
            }
        }

        // No result found, return empty array
        return response()->json([], 404);
    }
}
