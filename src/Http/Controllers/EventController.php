<?php

namespace AlphaIris\Events\Http\Controllers;

use AlphaIris\Core\Http\Controllers\AlphaIrisBreadController;
use AlphaIris\Events\Models\Event;
use AlphaIris\Events\Models\Ticket;
use AlphaIris\Events\Models\TicketRsvp;
use AlphaIris\Events\Models\TicketSale;
use Illuminate\Http\Request;
use TCG\Voyager\Facades\Voyager;
use Illuminate\Support\Str;

class EventController extends AlphaIrisBreadController
{
    protected function getSearchable()
    {
        return [
            'name' => 'Name',
        ];
    }

    protected function getData($request)
    {
        return Event::query();
    }

    protected function getSingleRecord($request, $id)
    {
        return Event::find($id);
    }

    protected function getNewRecord()
    {
        return new Event();
    }

    protected function getActionRoute($request, $action)
    {
        $slug = $this->getSlug($request);
        $actionName = $action->getPolicy();

        return function ($dataRow) use ($slug, $actionName) {
            switch ($actionName) {
                case 'edit':
                    return route('alphairis.'.$slug.'.'.$actionName, $dataRow);
                    break;
                case 'read':
                    return route('alphairis.'.$slug.'.show', $dataRow);
                    break;
                case 'delete':
                    return route('alphairis.'.$slug.'.destroy', $dataRow);
                    break;
                default:
                    return route('alphairis.'.$slug.'.'.$actionName);
            }
        };
    }

    protected function getBaseSlug()
    {
        return 'events';
    }

    protected function getPseudoDataType($slug)
    {
        $dt = parent::getPseudoDataType($slug);
        $dt->model_name = 'AlphaIris\Events\Models\Event';

        return $dt;
    }

    public function destroy(Request $request, $id)
    {
        $slug = $this->getSlug($request);
        $ogId = $id;

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Init array of IDs
        $ids = [];
        if (empty($id)) {
            // Bulk delete, get IDs from POST
            $ids = explode(',', $request->ids);
        } else {
            // Single item delete, get ID from URL
            $ids[] = $id;
        }

        foreach ($ids as $id) {
            $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);

            // Check permission
            $this->authorize('delete', $data);

            TicketSale::where('event_id', $id)->delete();
            TicketRsvp::where('event_id', $id)->delete();
            Ticket::where('event_id', $id)->delete();
        }

        return parent::destroy($request, $ogId);
    }

    public function replicate($event_id)
    {
        $event = Event::find($event_id);

        if ($event) {
            $replicated_event = $event->replicate();
            $replicated_event->name = $event->name . ' - copy';

            if ($event->eav_attributes->isNotEmpty()) {
                $fields = $event->eav_attributes->pluck('field');

                foreach ($fields as $field) {
                    $replicated_event->{$field} = $event->{$field};
                }
            }

            $replicated_event->uuid = Str::uuid();

            $replicated_event->save();

            if ($event->tickets->isNotEmpty()) {
                foreach ($event->tickets as $ticket) {
                    $replicated_ticket = $ticket->replicate();
                    $replicated_ticket->event_id = $replicated_event->id;
                    $replicated_ticket->name = $ticket->name . ' - copy';

                    $ticket_eav_attributes = $ticket->eav_attributes;

                    if ($ticket_eav_attributes->isNotEmpty()) {

                        $fields = $ticket_eav_attributes->pluck('field');

                        foreach ($fields as $field) {
                            $replicated_ticket->{$field} = $ticket->{$field};
                        }
                    }

                    $replicated_ticket->save();
                }
            }


            return redirect()->back()->with([
                'message' => 'Event replicated.',
            ]);
        } else {
            return redirect()->back();
        }
    }
}
