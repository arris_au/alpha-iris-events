<?php

namespace AlphaIris\Events\Http\Controllers;

use AlphaIris\Core\Http\Controllers\AlphaIrisBreadController;

class TicketRsvpController extends AlphaIrisBreadController
{
    public function insertUpdateData($request, $slug, $rows, $data)
    {
        $result = parent::insertUpdateData($request, $slug, $rows, $data);

        foreach ($result->eav_attributes as $eav) {
            $result->{$eav->field} = $request->get($eav->field);
        }
        $result->save();

        return $result;
    }
}
