<?php

namespace AlphaIris\Events\Http\Controllers;

use AlphaIris\Core\Http\Controllers\AlphaIrisBreadController;
use AlphaIris\Events\Models\EventField;
use Illuminate\Http\Request;
use TCG\Voyager\Facades\Voyager;

class EventFieldsController extends AlphaIrisBreadController
{
    /**
     * Get BREAD relations data.
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function relation(Request $request)
    {
        $slug = $this->getSlug($request);
        $page = $request->input('page');
        $search = $request->input('search', false);
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $method = $request->input('method', 'add');

        $model = app($dataType->model_name);
        if ($method != 'add') {
            $model = $model->find($request->input('id'));
        }

        $this->authorize($method, $model);

        $rows = $dataType->{$method.'Rows'};
        $row = $rows->where('field', $request->input('type'))->first();
        if ($row) {
            return $this->getRelationQuery($request, $dataType, $row, $search, $page);
        } else {
            // No result found, return empty array
            return response()->json([], 404);
        }
    }

    protected function getRelationQuery(Request $request, $dataType, $row, $search, $page)
    {
        $on_page = 50;

        $options = $row->details;
        $model = app($options->model);
        $skip = $on_page * ($page - 1);

        $additional_attributes = $model->additional_attributes ?? [];
        $eventId = null;
        $isTicketType = $request->input('type') == 'event_field_belongstomany_ticket_relationship';

        if ($isTicketType && $request->input('method') == 'edit') {
            $eventId = $dataType->model_name::find($request->input('id'))->event_id;
        }

        // Apply local scope if it is defined in the relationship-options
        if (isset($options->scope) && $options->scope != '' && method_exists($model, 'scope'.ucfirst($options->scope))) {
            $model = $model->{$options->scope}();
        }

        // If search query, use LIKE to filter results depending on field label
        if ($search) {
            // If we are using additional_attribute as label
            if (in_array($options->label, $additional_attributes)) {
                $relationshipOptions = $model->all();
                $relationshipOptions = $relationshipOptions->filter(function ($model) use ($search, $options) {
                    return stripos($model->{$options->label}, $search) !== false;
                });
                $total_count = $relationshipOptions->count();
                $relationshipOptions = $relationshipOptions->forPage($page, $on_page);
            } else {
                $total_count = $model->where($options->label, 'LIKE', '%'.$search.'%')->count();
                $relationshipOptions = $model->take($on_page)->skip($skip)
                    ->where($options->label, 'LIKE', '%'.$search.'%')
                    ->get();
            }
        } else {
            $total_count = $model->count();
            $relationshipOptions = $model->take($on_page)->skip($skip)->get();
        }

        if ($eventId) {
            $relationshipOptions = $relationshipOptions->where('event_id', $eventId);
        }

        $results = [];

        if (! $row->required && ! $search && $page == 1) {
            $results[] = [
                'id' => '',
                'text' => __('voyager::generic.none'),
            ];
        }

        // Sort results
        if (! empty($options->sort->field)) {
            if (! empty($options->sort->direction) && strtolower($options->sort->direction) == 'desc') {
                $relationshipOptions = $relationshipOptions->sortByDesc($options->sort->field);
            } else {
                $relationshipOptions = $relationshipOptions->sortBy($options->sort->field);
            }
        }

        foreach ($relationshipOptions as $relationshipOption) {
            if ($isTicketType) {
                $results[] = [
                    'id' => $relationshipOption->{$options->key},
                    'text' => $relationshipOption->event->name.' :: '.$relationshipOption->{$options->label},
                ];
            } else {
                $results[] = [
                    'id' => $relationshipOption->{$options->key},
                    'text' => $relationshipOption->{$options->label},
                ];
            }
        }

        return response()->json([
            'results' => $results,
            'pagination' => [
                'more' => ($total_count > ($skip + $on_page)),
            ],
        ]);
    }

    /**
     * Get the event fields for an event and ticket, each includes the rendered edit/add field html.
     *
     * @param \Illuminate\Http\Request $request
     * @param int|string $event_id
     * @param int|string $ticket_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function forEventTicket(Request $request, $event_id, $ticket_id)
    {
        $slug = $this->getSlug($request);
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
        $fields = EventField::forEvent($event_id)
            ->forTicket($ticket_id)
            ->get()
            ->map(function ($eventField) use ($dataType) {
                $eventField->html = view('voyager::bread.partials.edit-add-row', [
                    'row' => $eventField,
                    'dataType' => $dataType,
                    'dataTypeContent' => (object) [
                        $eventField->field.'_add' => null,
                    ],
                    'fieldName' => $eventField->field,
                    'edit' => false,
                ])->render();

                return $eventField;
            });

        return response()->json($fields);
    }
}
