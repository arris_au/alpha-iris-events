<?php

namespace AlphaIris\Events\Http\Controllers;

use AlphaIris\Core\Http\Controllers\AlphaIrisBreadController;
use AlphaIris\Events\Models\Ticket;
use AlphaIris\Events\Models\TicketSale;
use Illuminate\Http\Request;

class TicketController extends AlphaIrisBreadController
{
    public function store(Request $request)
    {
        $request = $request->merge([
            'allow_any_member' => ($request->get('allow_any_member') == '1'),
        ]);
        parent::store($request);

        return  redirect()->route('voyager.tickets.edit', $this->lastInsert);
    }

    public function show(Request $request, $id)
    {
        if ($request->ajax()) {
            $ticket = Ticket::findOrFail($id);

            return response()->json($ticket);
        }

        return parent::show($request, $id);
    }

    public function update(Request $request, $id)
    {
        $request = $request->merge([
            'allow_any_member' => ($request->get('allow_any_member') == '1'),
        ]);
        parent::update($request, $id);

        return  redirect()->route('voyager.tickets.edit', $id);
    }

    public function destroy(Request $request, $id)
    {
        if (TicketSale::where('ticket_id', $id)->count() > 0) {
            return redirect()->route('voyager.tickets.index')
                ->with([
                    'message' => 'There are already sales for this ticket. Ticket can not be deleted.',
                    'alert-type' => 'error',
                ]);
        }

        return parent::destroy($request, $id);
    }
}
