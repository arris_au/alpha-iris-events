<?php

namespace AlphaIris\Events\Http\Controllers\Frontend;

use AlphaIris\Core\Helpers\DummyTypeContent;
use AlphaIris\Core\Services\PublicFieldsService;
use AlphaIris\Events\Http\Requests\EventRegistrationRequest;
use AlphaIris\Events\Models\Event;
use AlphaIris\Events\Models\EventField;
use AlphaIris\Events\Models\Ticket;
use AlphaIris\Events\Models\TicketRsvp;
use AlphaIris\Events\Models\TicketSale;
use AlphaIris\Payments\Services\Payments;
use AlphaIris\Payments\Services\PaymentsService;
use AlphaIris\Shopping\Events\OrderCreatedEvent;
use AlphaIris\Shopping\Events\OrderPaidEvent;
use AlphaIris\Shopping\Models\Order;
use AlphaIris\Shopping\Models\OrderStatus;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use TCG\Voyager\Models\DataType;

class EventController extends Controller
{
    public function index()
    {
        $Events = Event::where('event_start', '>', new Carbon())->where('is_private', 0)->get();

        return view('alpha-iris-events::index', ['events' => $Events]);
    }

    public function show(Event $event)
    {
        return view('alpha-iris-events::show', ['event' => $event]);
    }

    public function store(Request $request, Event $event)
    {
        $this->removeEventTickets($event);

        $ticketsAdded = 0;
        foreach ($event->tickets as $ticketType) {
            $qty = $request->get('ticket_'.$ticketType->id, 0);
            if ($qty > 0 && $ticketType->eligible) {
                if ($ticketType->qtyAvailable($qty)) {
                    $ticketsAdded += $qty;
                    Ticket::addToCart($ticketType->id, $qty);
                } else {
                    $request->validate([
                        'ticket_'.$ticketType->id => function ($attribute, $value, $fail) use ($ticketType) {
                            return $fail('You have requested more '.$ticketType->name.' than are available');
                        },
                    ]);
                }
            }
        }
        Validator::extendImplicit('tickets_required', function ($attribute, $value, $fail) {
            return false;
        }, 'You have not selected any tickets');

        if ($ticketsAdded == 0) {
            $request->validate(['tickets_required']);
        }

        return redirect(route('events.show_cart', $event));
    }

    public function cart(Event $event)
    {
        return view('alpha-iris-events::cart', $this->getCartViewParams($event));
    }

    public function register(EventRegistrationRequest $request, Event $event)
    {
        $request = $request->merge(['event_id' => $event->id]);
        $request->validate(['payment_method' => 'required']);

        $method_key = $request->get('payment_method');
        $method = Payments::availableMethods()->filter(fn ($method) => $method->getKey() == $method_key)->first();
        $method->validate($request);

        $pending = OrderStatus::where('name', 'Pending')->firstOrFail();
        $totalDue = 0;
        $regIds = [];
        $data = $request->all();
        $order = Order::create([
            'user_id' => optional(auth()->user())->id,
            'order_status_id' => $pending->id,
            'company' => $request->get('company'),
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'email' => $request->get('email'),
            'phone' => $request->get('phone'),
            'address_1' => $request->get('address_1'),
            'address_2' => $request->get('address_2'),
            'suburb' => $request->get('suburb'),
            'state_id' => $request->get('state_id'),
            'postcode' => $request->get('postcode'),
            'country_id' => $request->get('country_id'),
            'shipping_first_name' => $request->get('first_name'),
            'shipping_last_name' => $request->get('last_name'),
            'shipping_address_1' => $request->get('address_1'),
            'shipping_address_2' => $request->get('address_2'),
            'shipping_suburb' => $request->get('suburb'),
            'shipping_state_id' => $request->get('state_id'),
            'shipping_postcode' => $request->get('postcode'),
            'shipping_country_id' => $request->get('country_id'),
        ]);

        foreach ($this->getCartTickets($event) as $cartTicket) {
            $totalDue += $cartTicket['line_total'];
            $allFields = $this->getTicketSalePublicFields($event->id, collect([$cartTicket]));
            for ($i = 0; $i < $cartTicket['quantity']; $i++) {
                $ticketSale = new TicketSale([
                    'event_id' => $event->id,
                    'ticket_id' => $cartTicket['modelId'],
                    'user_id' => optional(auth()->user())->id,
                    'ticket_price' => $cartTicket['price'],
                ]);

                foreach ($allFields as $field) {
                    $fieldName = $field->field;
                    if ($field->type == 'relationship') {
                        $fieldName = $field->details->column;
                    }
                    $ticketSale->$fieldName = $data[$fieldName] ?? null;
                }
                $ticketSale->save();
                $regIds[] = $ticketSale->id;
                $order->addItem($ticketSale, $cartTicket['name'], $ticketSale->ticket_price);
            }
        }
        $this->removeEventTickets($event);
        $order->calculateTotals();
        $payment_status = $method->process($request, $order->grand_total, $order->id);
        $order->payment_status = $payment_status;
        $order->save();
        OrderCreatedEvent::dispatch($order);
        if ($payment_status == PaymentsService::PAYMENT_CLEARED) {
            $order->payment_status = PaymentsService::PAYMENT_CLEARED;
            if (setting('shopping.order_paid_status')) {
                $order->order_status_id = setting('shopping.order_paid_status');
            }
            $order->save();

            if ($order->items()->where('model_class', TicketSale::class)->exists()) {
                $item_ids = $order->items->where('model_class', TicketSale::class)->pluck('model_id');

                TicketSale::whereIn('id', $item_ids)->update([
                    'order_payment_status' => PaymentsService::PAYMENT_CLEARED,
                ]);
            }

            OrderPaidEvent::dispatch($order);
        }

        Session::flash('message', 'Thank you for your order!');
        Session::flash('alert-class', 'alert-success');

        return redirect()->route('shopping.order.show', $order->id);
    }

    public function rsvp(EventRegistrationRequest $request, Event $event)
    {
        $request = $request->merge(['event_id' => $event->id]);
        $data = $request->all();

        $ticketRsvp = new TicketRsvp([
            'event_id' => $event->id,
            'user_id' => optional(auth()->user())->id,
        ]);

        $allFields = $this->getTicketRSVPPublicFields($event->id);

        foreach ($allFields as $field) {
            $fieldName = $field->field;
            if ($field->type == 'relationship') {
                $fieldName = $field->details->column;
            }
            $ticketRsvp->$fieldName = $data[$fieldName] ?? null;
        }
        $ticketRsvp->save();

        return view('alpha-iris-events::rsvp-thankyou', [
            'event' => $event,
            'rsvp' => $ticketRsvp,
        ]);
    }

    public function showRsvp(Event $event)
    {
        if ($event->userSentRsvp()) {
            return redirect()->route('events.show', $event)->with([
                'message' => 'You have already sent an RSVP for this event.',
                'message-class' => 'alert-error',
            ]);
        }

        return view('alpha-iris-events::rsvp', $this->getCartViewParams($event, true));
    }

    public function getCartTickets(Event $event)
    {
        $allItems = cart()->items();
        $items = collect();
        foreach ($allItems as $item) {
            if ($item['modelType'] == Ticket::class) {
                $ticket = Ticket::find($item['modelId']);
                if ($ticket && $ticket->event->id == $event->id) {
                    $roundedPrice = round($item['price_inc'], 2);

                    $item['display_price'] = Payments::formatCurrency($roundedPrice);
                    $item['line_total'] = $roundedPrice * $item['quantity'];
                    $items->add($item);
                }
            }
        }

        return $items;
    }

    public function getCartViewParams(Event $event, $isRSVP = false)
    {
        $items = $this->getCartTickets($event);
        $ticketsTotal = $items->sum('line_total');
        $dtc = new DummyTypeContent();
        if (auth()->user()) {
            $user = auth()->user();
            $dtc->first_name = $user->name;
            $dtc->last_name = $user->lastname;
            $dtc->email = $user->email;
            $dtc->phone = $user->phone;
            $dtc->address_1 = $user->address_1;
            $dtc->address_2 = $user->address_2;
            $dtc->suburb = $user->suburb;
            $dtc->state_id = $user->state_id;
            $dtc->postcode = $user->post_code;
            $dtc->country_id = $user->country_id;
        }

        if ($isRSVP) {
            $dataTypeName = 'ticket_rsvps';
            $fields = $this->getTicketRSVPPublicFields($event->id);
        } else {
            $dataTypeName = 'ticket_sales';
            $fields = $this->getTicketSalePublicFields($event->id, $items);
        }

        $order = new \stdClass();
        $order->id = 'tickets';
        $order->grand_total = $ticketsTotal;

        return [
            'ticketsTotal' => $ticketsTotal,
            'displayTotal' => Payments::formatCurrency($ticketsTotal),
            'event' => $event,
            'items' => $items,
            'fields' => $fields,
            'dataType' => DataType::where('name', $dataTypeName)->first(),
            'dataTypeContent' => $dtc,
            'methods' => Payments::availableMethods(),
            'request' => request(),
            'order' => $order,
        ];
    }

    protected function getTicketRSVPPublicFields($eventId)
    {
        return $this->getPublicFields('ticket_rsvps', $eventId);
    }

    protected function getTicketSalePublicFields($eventId, $items = null)
    {
        return $this->getPublicFields('ticket_sales', $eventId, $items);
    }

    /**
     * Get the public fields for the specified event and ticket items.
     *
     * @param string $dataTypeName
     * @param int $eventId
     * @param \Illuminate\Support\Collection|null $items
     * @return \Illuminate\Support\Collection
     */
    protected function getPublicFields($dataTypeName, $eventId, $items = null)
    {
        if (is_null($items)) {
            $items = collect();
        }
        $ticketsDT = DataType::where('name', $dataTypeName)->first();
        $allFields = PublicFieldsService::fields($ticketsDT);

        $eventFields = EventField::where('event_id', $eventId)
            ->when($items->isNotEmpty(), function ($q) use ($items) {
                $q->whereHas('tickets', function ($q) use ($items) {
                    $q->whereIn('ticket_id', $items->pluck('modelId')->toArray());
                });
            })
            ->get();

        $allFields = $allFields->merge($eventFields);

        return $allFields;
    }

    protected function removeEventTickets($event)
    {
        $allItems = cart()->items();
        $indexes = [];

        foreach ($allItems as $index => $item) {
            if ($item['modelType'] == Ticket::class) {
                $item['model'] = Ticket::find($item['modelId']);
                if ($item['model']->event->id == $event->id) {
                    $indexes[] = $index;
                }
            }
        }
        asort($indexes);
        for ($i = count($indexes) - 1; $i >= 0; $i--) {
            cart()->removeAt($indexes[$i]);
        }
    }
}
