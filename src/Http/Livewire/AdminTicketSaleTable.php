<?php

namespace AlphaIris\Events\Http\Livewire;

use AlphaIris\Core\Datatables\DataTypeColumn;
use AlphaIris\Core\Http\Livewire\AlphaIrisAdminTable;
use AlphaIris\Events\Models\TicketSale;
use Carbon\Carbon;
use Mediconesystems\LivewireDatatables\Column;

class AdminTicketSaleTable extends AlphaIrisAdminTable
{
    public $eventId;

    public function builder()
    {
        $query = TicketSale::query();
        if ($this->eventId) {
            $query = $query->where('ticket_sales.event_id', $this->eventId);
        }

        return $query;
    }

    protected function createColumn($row)
    {
        if ($row->field == 'checked_in') {
            return $this->getCheckInColumn($row);
        }

        $column = new DataTypeColumn($this->dataTypeModel, $row);

        if ($row->field == 'ticket_sale_belongsto_event_relationship' && $this->eventId) {
            return $column;
        }

        if ($this->canSearch) {
            $filter_options = null;
            if ($row->type == 'relationship' && $row->field != 'ticket_sale_belongsto_user_relationship') {
                $filter_options = $row->details->model::pluck('name', 'id');
            } elseif ($row->type == 'select_dropdown') {
                $filter_options = [];
                foreach ((array) $row->details->options as $id => $name) {
                    $filter_options[] = compact('id', 'name');
                }
                $column->callback = function ($value) use ($row) {
                    return $row->details->options->$value;
                };
            }
            $column = $column->searchable()->filterable($filter_options);
        }

        return $column;
    }

    protected function getCheckInColumn($row)
    {
        return Column::callback('id', function ($id) {
            $model = TicketSale::find($id);
            if (auth()->user()->can('edit', $model)) {
                $title = $model->checked_in ? 'Check Out' : 'Check In';
                $voyagerClass = $model->checked_in ? 'voyager-double-left' : 'voyager-double-right';
                $btnClass = $model->checked_in ? 'btn-danger' : 'btn-primary';
                $html = '<button type="button" wire:click="checkIn('.$id.')" ';
                $html .= '" title="'.$title.'" class="btn btn-sm '.$btnClass.' edit">';
                $html .= '<i class="'.$voyagerClass.'"></i> <span class="hidden-xs hidden-sm">'.$title.'</span></button>';
            } else {
                $html = $model->checked_in ? 'Yes' : 'No';
            }

            return $html;
        })->label('Check In');
    }

    public function checkIn($id)
    {
        $model = TicketSale::find($id);
        $model->checked_in = $model->checked_in ? null : new Carbon();
        $model->save();
    }
}
