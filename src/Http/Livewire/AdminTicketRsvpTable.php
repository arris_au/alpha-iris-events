<?php

namespace AlphaIris\Events\Http\Livewire;

use AlphaIris\Core\Datatables\DataTypeColumn;
use AlphaIris\Core\Http\Livewire\AlphaIrisAdminTable;
use AlphaIris\Events\Models\TicketRsvp;
use Carbon\Carbon;
use Mediconesystems\LivewireDatatables\Column;

class AdminTicketRsvpTable extends AlphaIrisAdminTable
{
    public $eventId;

    public function mount($model = null, $include = [], $exclude = [], $hide = [], $dates = [], $times = [], $searchable = [], $sort = null, $hideHeader = null, $hidePagination = null, $perPage = null, $exportable = false, $hideable = false, $beforeTableSlot = false, $afterTableSlot = false, $dataType = null, $params = [], $eventId = null)
    {
        parent::mount($model = null, $include = [], $exclude = [], $hide = [], $dates = [], $times = [], $searchable = [], $sort, $hideHeader, $hidePagination, $perPage, $exportable = true, $hideable, $beforeTableSlot, $afterTableSlot, $dataType, $params);
        $this->eventId = $eventId;
    }

    public function builder()
    {
        $query = TicketRsvp::query();
        if ($this->eventId) {
            $query = $query->where('ticket_rsvps.event_id', $this->eventId);
        }

        return $query;
    }

    protected function createColumn($row)
    {
        switch ($row->field) {
            case 'checked_in':
                return $this->getCheckInColumn($row);
                break;
            default:
                return new DataTypeColumn($this->dataTypeModel, $row);
        }
    }

    protected function getCheckInColumn($row)
    {
        return Column::callback('id', function ($id) {
            $model = TicketRsvp::find($id);
            if (auth()->user()->can('edit', $model)) {
                $routeName = 'voyager.ticket-sales.checkin';
                $title = $model->checked_in ? 'Check Out' : 'Check In';
                $voyagerClass = $model->checked_in ? 'voyager-double-left' : 'voyager-double-right';
                $btnClass = $model->checked_in ? 'btn-danger' : 'btn-primary';
                $html = '<button type="button" wire:click="checkIn('.$id.')" ';
                $html .= '" title="'.$title.'" class="btn btn-sm '.$btnClass.' edit">';
                $html .= '<i class="'.$voyagerClass.'"></i> <span class="hidden-xs hidden-sm">'.$title.'</span></button>';
            } else {
                $html = $model->checked_in ? 'Yes' : 'No';
            }

            return $html;
        })->label('Check In');
    }

    public function checkIn($id)
    {
        $model = TicketRsvp::find($id);
        $model->checked_in = $model->checked_in ? null : new Carbon();
        $model->save();
    }
}
