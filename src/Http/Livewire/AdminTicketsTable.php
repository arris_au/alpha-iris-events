<?php

namespace AlphaIris\Events\Http\Livewire;

use AlphaIris\Core\Http\Livewire\AlphaIrisAdminTable;
use AlphaIris\Events\Models\Ticket;

class AdminTicketsTable extends AlphaIrisAdminTable
{
    public $eventId;

    public function mount($model = null, $include = [], $exclude = [], $hide = [], $dates = [], $times = [], $searchable = [], $sort = null, $hideHeader = null, $hidePagination = null, $perPage = null, $exportable = false, $hideable = false, $beforeTableSlot = false, $afterTableSlot = false, $dataType = null, $params = [], $eventId = null)
    {
        parent::mount($model = null, $include = [], $exclude = [], $hide = [], $dates = [], $times = [], $searchable = [], $sort = null, $hideHeader = null, $hidePagination = null, $perPage = null, $exportable = false, $hideable = false, $beforeTableSlot = false, $afterTableSlot = false, $dataType = null, $params = []);
        $this->eventId = $eventId;
    }

    public function builder()
    {
        $query = Ticket::query();
        if ($this->eventId) {
            $query = $query->where('event_id', $this->eventId);
        }

        return $query;
    }
}
