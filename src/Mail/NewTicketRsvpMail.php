<?php

namespace AlphaIris\Events\Mail;

use AlphaIris\Core\Models\User;
use AlphaIris\Events\Models\TicketRsvp;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use PDF;

class NewTicketRsvpMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $ticketRsvp;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(TicketRsvp $ticketRsvp)
    {
        $this->ticketRsvp = $ticketRsvp;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->getSubject())->view('alpha-iris-events::emails.ticket_rsvp', ['ticketRsvp' => $this->ticketRsvp]);
    }

    protected function getSubject()
    {
        return 'Your RSVP to '.$this->ticketRsvp->event->name;
    }
}
