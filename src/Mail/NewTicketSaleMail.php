<?php

namespace AlphaIris\Events\Mail;

use AlphaIris\Core\Models\User;
use AlphaIris\Events\Models\TicketRsvp;
use AlphaIris\Events\Models\TicketSale;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use PDF;

class NewTicketSaleMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $ticketSale;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(TicketSale $ticketSale)
    {
        $this->ticketSale = $ticketSale;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->getSubject())->view('alpha-iris-events::emails.ticket_sale', ['ticketSale' => $this->ticketSale]);
    }

    protected function getSubject()
    {
        return 'Your tickets to '.$this->ticketSale->event->name;
    }
}
