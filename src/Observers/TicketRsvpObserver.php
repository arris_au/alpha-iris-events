<?php

namespace AlphaIris\Events\Observers;

use AlphaIris\Events\Mail\NewTicketRsvpMail;
use AlphaIris\Events\Models\TicketRsvp;
use Illuminate\Support\Facades\Mail;

class TicketRsvpObserver
{
    /**
     * Handle the TicketSale "created" event.
     *
     * @param  \AlphaIris\Events\Models\TicketRsvp  $ticketRsvp
     * @return void
     */
    public function created(TicketRsvp $ticketRsvp)
    {
        if (setting('events.send_rsvp_email')) {
            $mailer = Mail::to($ticketRsvp->email);
            if (setting('events.cc_site_owner') && ! (setting('site.email') !== '')) {
                $mailer->cc(setting('site.email'));
            }

            $mailable = app()->make(NewTicketRsvpMail::class, ['ticketRsvp' => $ticketRsvp]);
            $mailer->send($mailable);
        }
    }

    /**
     * Handle the TicketRsvp "updated" event.
     *
     * @param  \AlphaIris\Events\Models\TicketRsvp  $ticketRsvp
     * @return void
     */
    public function updated(TicketRsvp $ticketRsvp)
    {
    }

    /**
     * Handle the User "deleted" event.
     *
     * @param  AlphaIris\Events\Models\TicketRsvp  $ticketRsvp
     * @return void
     */
    public function deleted(TicketRsvp $ticketRsvp)
    {
        //
    }

    /**
     * Handle the TicketRsvp "forceDeleted" event.
     *
     * @param  AlphaIris\Events\Models\TicketRsvp  $ticketRsvp
     * @return void
     */
    public function forceDeleted(TicketRsvp $ticketRsvp)
    {
        //
    }
}
