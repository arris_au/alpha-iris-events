<?php

namespace AlphaIris\Events\Observers;

use AlphaIris\Events\Mail\NewTicketSaleMail;
use AlphaIris\Events\Models\TicketSale;
use Illuminate\Support\Facades\Mail;

class TicketSaleObserver
{
    /**
     * Handle the TicketSale "created" event.
     *
     * @param  \AlphaIris\Events\Models\TicketSale  $ticketSale
     * @return void
     */
    public function created(TicketSale $ticketSale)
    {
        if (setting('events.send_sales_email')) {
            $mailable = Mail::to($ticketSale->email);
            if (setting('events.cc_site_owner') && ! (setting('site.email') !== '')) {
                $mailable->cc(setting('site.email'));
            }
            $mailable->send(app()->make(NewTicketSaleMail::class, ['ticketSale' =>$ticketSale]));
        }
    }

    /**
     * Handle the TicketSale "updated" event.
     *
     * @param  \AlphaIris\Events\Models\TicketSale  $ticketSale
     * @return void
     */
    public function updated(TicketSale $ticketSale)
    {
    }

    /**
     * Handle the User "deleted" event.
     *
     * @param  AlphaIris\Events\Models\TicketSale  $ticketSale
     * @return void
     */
    public function deleted(TicketSale $ticketSale)
    {
        //
    }

    /**
     * Handle the TicketSale "forceDeleted" event.
     *
     * @param  AlphaIris\Events\Models\TicketSale  $ticketSale
     * @return void
     */
    public function forceDeleted(TicketSale $ticketSale)
    {
        //
    }
}
